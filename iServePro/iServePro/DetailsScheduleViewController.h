//
//  DetailsScheduleViewController.h
//  Onthewaypro
//
//  Created by Rahul Sharma on 02/11/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailsScheduleViewController : UIViewController

@property (strong, nonatomic) IBOutlet UILabel *addressLabel;

- (IBAction)goForNewAddress:(id)sender;
@property (strong, nonatomic) IBOutlet UITextField *pricePerTime;

@property (strong, nonatomic) IBOutlet UITextField *distanceInRadius;
@property (strong, nonatomic) IBOutlet UIButton *repeatMode;
- (IBAction)repeatMode:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *nonRepeat;
- (IBAction)nonRepeat:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *startDate;
- (IBAction)startDate:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *endDate;
- (IBAction)endDate:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *startTime;
- (IBAction)startTime:(id)sender;

@property (strong, nonatomic) IBOutlet UICollectionView *slotsCollectionView;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *bottomSpace;
@property (strong, nonatomic) IBOutlet UIDatePicker *datePicker;
- (IBAction)cancelPicker:(id)sender;

@property (strong, nonatomic) IBOutlet UILabel *selectType;
- (IBAction)donePicker:(id)sender;

@property (strong, nonatomic) IBOutlet UIView *pickerView;
@property (strong, nonatomic) IBOutlet UIButton *confirmSlot;
- (IBAction)confirmSlot:(id)sender;
@property (strong, nonatomic) NSMutableArray *slotsArray;



@property (strong, nonatomic) IBOutlet UIButton *everyDa;

- (IBAction)everyDa:(id)sender;

@property (strong, nonatomic) IBOutlet UIButton *weekDay;
- (IBAction)weekDay:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *weekEnd;
- (IBAction)weekEnd:(id)sender;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *heightOfweekDay;
@property (strong, nonatomic) IBOutlet UILabel *endDateLabe;
//@property (strong, nonatomic) IBOutlet NSLayoutConstraint *heightOfTimes;
@property (strong, nonatomic) IBOutlet UITextField *noOfslot;
@property (strong, nonatomic) IBOutlet UITextField *slotMinute;

@end
