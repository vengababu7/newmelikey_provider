//
//  NetworkHandler.m
//  Iserve
//

//  Copyright (c) 2014 Rahul Sharma. All rights reserved.
//

#import "NetworkHandler.h"

//#import <AFNetworking/AFHTTPRequestOperationManager.h>
#import "Errorhandler.h"
#import <AFHTTPSessionManager.h>




@interface NetworkHandler()
@property(nonatomic,strong)  AFHTTPSessionManager *httpClient;
@end
@implementation NetworkHandler

static NetworkHandler *networkHandler;

+ (id)sharedInstance {
    if (!networkHandler) {
        networkHandler  = [[self alloc] init];
    }
    
    return networkHandler;
}




-(void)composeRequestWithMethod:(NSString*)method paramas:(NSDictionary*)paramas onComplition:(void (^)(BOOL succeeded, NSDictionary  *response))completionBlock
{
    
    _httpClient = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:BASE_URL]];
    //   self.httpClient.requestSerializer = [AFJSONRequestSerializer serializer];
    _httpClient.responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingAllowFragments|NSJSONReadingMutableContainers];
    
    NSString *url = [NSString stringWithFormat:@"%@%@",BASE_URL,method];
    [_httpClient POST:url
           parameters:paramas
             progress:nil
              success:^(NSURLSessionTask *task, id responseObject) {
                  
                  
                  completionBlock(YES,responseObject);
                  
                  NSLog(@"JSON: %@", responseObject);
                  //send success response with data
                  
              } failure:^(NSURLSessionTask *operation, NSError *error) {
                  
                  [[ProgressIndicator sharedInstance] hideProgressIndicator];
                  UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Network Error",@"Network Error") message:[error localizedDescription] delegate:nil cancelButtonTitle:NSLocalizedString(@"OK",@"OK") otherButtonTitles:nil, nil];
                  [alertView show];
                  
                  completionBlock(NO,nil);
                  
                  
              }];
}


-(NSString*)getBaseStringWithMethod:(NSString*)method
{
    return [NSString stringWithFormat:@"%@%@", BASE_URL, method];
}

-(NSString*)getBaseUrlString
{
    return [NSString stringWithFormat:@"%@", BASE_URL_RESTKIT];
}


-(NSString*)paramDictionaryToString:(NSDictionary*)params
{
    NSMutableString *request = [[NSMutableString alloc] init];
    [params enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
        [request appendFormat:@"&%@=%@", key, obj];
    }];
    
    NSString *finalRequest = request;
    if ([request hasPrefix:@"&"]) {
        finalRequest = [request substringFromIndex:1];
    }
    
    return finalRequest;
}

-(void)cancelRequest{
    
    [[_httpClient operationQueue] cancelAllOperations];
}

@end
