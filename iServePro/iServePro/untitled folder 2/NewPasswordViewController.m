//
//  NewPasswordViewController.m
//  Onthewaypro
//
//  Created by Rahul Sharma on 01/11/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import "NewPasswordViewController.h"
#import "iServeSplashController.h"

#define REGEX_PASSWORD_ONE_UPPERCASE @"^(?=.*[A-Z]).*$"  //Should contains one or more uppercase letters
#define REGEX_PASSWORD_ONE_LOWERCASE @"^(?=.*[a-z]).*$"  //Should contains one or more lowercase letters
#define REGEX_PASSWORD_ONE_NUMBER @"^(?=.*[0-9]).*$"  //Should contains one or more number
#define REGEX_PASSWORD_ONE_SYMBOL @"^(?=.*[!@#$%&_]).*$"  //Should contains one or more symbol
@interface NewPasswordViewController ()

@end

@implementation NewPasswordViewController

- (void)viewDidLoad {
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(dismissKeyboard)];
    [self.view addGestureRecognizer:tapGesture];
    [super viewDidLoad];
    _submitButton.layer.borderWidth = 2;
    _submitButton.layer.borderColor = UIColorFromRGB(0X008000).CGColor;
    [_password becomeFirstResponder];
    self.title = @"Change Password";
    [self createNavLeftButton];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)dismissKeyboard{
    [self.view endEditing:YES];
}
/*-----------------------------------*/
#pragma mark - Navigation Bar methods
/*-----------------------------------*/
-(void) createNavLeftButton
{
    UIImage *buttonImageNavNormal =[UIImage imageNamed:@"bnt_bck_normal.png"];
    UIImage *buttonImageNavHigh = [UIImage imageNamed:@"bnt_bck_pressed.png"];
    UIButton *cancelButton =  [UIButton buttonWithType:UIButtonTypeCustom];
    [cancelButton addTarget:self action:@selector(backButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    [cancelButton setFrame:CGRectMake(0.0f,0.0f,buttonImageNavHigh.size.width,buttonImageNavHigh.size.height)];
    [cancelButton setFrame:CGRectMake(0.0f,0.0f,buttonImageNavNormal.size.width,buttonImageNavNormal.size.height)];
    [cancelButton setBackgroundImage:buttonImageNavHigh forState:UIControlStateHighlighted];
    [cancelButton setImage:buttonImageNavNormal forState:UIControlStateNormal];
    UIBarButtonItem *containingcancelButton = [[UIBarButtonItem alloc]
                                               initWithCustomView:cancelButton];
    UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc]
                                       initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace
                                       target:nil action:nil];
    negativeSpacer.width = -5;// it was -6 in iOS 6  you can set this as per your preference
    [self.navigationItem setLeftBarButtonItems:[NSArray arrayWithObjects:negativeSpacer,containingcancelButton, nil] animated:NO];
}
-(void)backButtonPressed
{
    [self.navigationController popViewControllerAnimated:YES];
    
    [self.view endEditing:YES];
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma mark - UITextFeildDelegate

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
  return YES;
}
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    if(textField == _conPassword)
    {
        int strength = [self checkPasswordStrength:_password.text];
        if(strength == 0)
        {
            [_password becomeFirstResponder];
        }
    }
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
//    if (textField == _conPassword) {
//        if ([_conPassword.text isEqualToString:_password.text]) {
//            [self.view endEditing:YES];
//        }else{
//            [Helper showAlertWithTitle:@"Message" Message:@"password Mismatched"];
//        }
//    }
 }

- (BOOL)textFieldShouldClear:(UITextField *)textField
{
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField == _password) {
         [_conPassword becomeFirstResponder];
    }
   else if(textField == _conPassword)
    {
        if ([_conPassword.text isEqualToString:_password.text]) {
            [self.view endEditing:YES];
        }else{
            [Helper showAlertWithTitle:@"Message" Message:@"password Mismatched"];
            [_conPassword becomeFirstResponder];
        }
    }
    return YES;
}
#pragma mark - Password Checking

-(int)checkPasswordStrength:(NSString *)password
{
    unsigned long int  len = password.length;
    //will contains password strength
    int strength = 0;
    
    if (len == 0) {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Message",@"Message") message:NSLocalizedString(@"Please enter password first",@"Please enter password first") delegate:self cancelButtonTitle:NSLocalizedString(@"Ok",@"Ok") otherButtonTitles:nil, nil];
        alert.tag=1;
        [alert show];
        
        return 0;//PasswordStrengthTypeWeak;
    } else if (len <= 5) {
        strength++;
    } else if (len <= 10) {
        strength += 2;
    } else{
        strength += 3;
    }
    int kp = strength;
    strength += [self validateString:password withPattern:REGEX_PASSWORD_ONE_UPPERCASE caseSensitive:YES];
    if (kp >= strength)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Message",@"Message") message:NSLocalizedString(@"Please enter atleast one Uppercase alphabet",@"Please enter atleast one Uppercase alphabet") delegate:self cancelButtonTitle:NSLocalizedString(@"Ok",@"Ok") otherButtonTitles:nil, nil];
        alert.tag=1;
        [alert show];
        return 0;
    }
    else
    {
        kp++;
    }
    strength += [self validateString:password withPattern:REGEX_PASSWORD_ONE_LOWERCASE caseSensitive:YES];
    if (kp >= strength)
    {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Message",@"Message") message:NSLocalizedString(@"Please enter atleast one Lowercase alphabet",@"Please enter atleast one Lowercase alphabet") delegate:self cancelButtonTitle:NSLocalizedString(@"Ok",@"Ok") otherButtonTitles:nil, nil];
        alert.tag=1;
        [alert show];
        return 0;
    }
    else
    {
        kp++;
    }
    strength += [self validateString:password withPattern:REGEX_PASSWORD_ONE_NUMBER caseSensitive:YES];
    if (kp >= strength) {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Message",@"Message") message:NSLocalizedString(@"Please enter atleast  one Number",@"Please enter atleast  one Number") delegate:self cancelButtonTitle:NSLocalizedString(@"Ok",@"Ok") otherButtonTitles:nil, nil];
        alert.tag=1;
        [alert show];
        
        return 0;
    }
    return 1;
}

- (int)validateString:(NSString *)string withPattern:(NSString *)pattern caseSensitive:(BOOL)caseSensitive
{
    NSError *error = nil;
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:pattern options:((caseSensitive) ? 0 : NSRegularExpressionCaseInsensitive) error:&error];
    
    NSAssert(regex, @"Unable to create regular expression");
    
    NSRange textRange = NSMakeRange(0, string.length);
    //NSLog(@"test range %ld",textRange);
    NSRange matchRange = [regex rangeOfFirstMatchInString:string options:NSMatchingReportProgress range:textRange];
    
    BOOL didValidate = 0;
    
    // Did we find a matching range
    if (matchRange.location != NSNotFound)
        didValidate = 1;
    
    return didValidate;
}

- (IBAction)submitThePassword:(id)sender {
    [self.view endEditing:YES];
    if (_password.text.length > 0) {
        if ([_conPassword.text isEqualToString:_password.text]) {
            [self changeThePasswordWebservcie];
        }else{
            [Helper showAlertWithTitle:@"Message" Message:@"password Mismatched"];
            [_conPassword becomeFirstResponder];
        }
    }else{
        [Helper showAlertWithTitle:@"Message" Message:@"Enter the Password"];
        [_password becomeFirstResponder];
    }
}

-(void)changeThePasswordWebservcie{
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    [[ProgressIndicator sharedInstance] showPIOnView:self.view withMessage:@"updating password.."];
    NSDictionary *dict = @{
                           @"ent_mobile" : [ud objectForKey:@"mobileNO"],
                           @"ent_pass":_password.text,
                           @"ent_user_type" : @"1"
                           };
    NetworkHandler *handler =[NetworkHandler sharedInstance];
    [handler composeRequestWithMethod:@"updatePasswordForUser"
                              paramas:dict
                         onComplition:^(BOOL succeeded, NSDictionary *response) {
                             if (succeeded)
                             {
                                 [Helper showAlertWithTitle:@"Message" Message:response[@"errMsg"]];
                                 [[ProgressIndicator sharedInstance] hideProgressIndicator];
                                 if ([ud boolForKey:@"fromProfile"]) {
                                     [self.navigationController popViewControllerAnimated:YES];
                                 }else{
                                     [self donePasswordChange];
                                 }
                             }
                         }];
}
-(void)donePasswordChange
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:
                                @"Main" bundle:[NSBundle mainBundle]];
    iServeSplashController *splah = [storyboard instantiateViewControllerWithIdentifier:@"splash"];
    self.navigationController.viewControllers = [NSArray arrayWithObjects:splah, nil];
}

@end
