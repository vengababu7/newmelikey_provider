//
//  CustomerRatingPOPUP.m
//  iServePro
//
//  Created by Rahul Sharma on 03/05/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import "CustomerRatingPOPUP.h"
#import "CancelReasonTableViewCell.h"

@implementation CustomerRatingPOPUP

-(id)init{
    self = [[[NSBundle mainBundle] loadNibNamed:@"CustomerRating"
                                          owner:self
                                        options:nil] firstObject];
    [self.cancelTableView registerNib:[UINib nibWithNibName:@"CancelReason" bundle:nil] forCellReuseIdentifier:@"cancel"];
    return self;
}
-(void) onWindow:(UIWindow *)onWindow
{
    _selectedIndex = -1;
      _isSelected=NO;
    _cancelReasons = [[NSMutableArray alloc]init];
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(dimissView)];
    tapGesture.delegate = self;
    [self addGestureRecognizer:tapGesture];
    self.frame = onWindow.frame;
    [onWindow addSubview:self];
    
    [self layoutIfNeeded];
    self.contentView.alpha = 0.3;
    
    [UIView animateWithDuration:0.3
                     animations:^{
                         self.contentView.alpha = 1;
                     }
                     completion:^(BOOL finished) {
                         [self reasonForCancelBookingApi];
                     }];
    
}

-(void)reasonForCancelBookingApi{
   
    [[ProgressIndicator sharedInstance] showPIOnView:self withMessage:@"Loading.."];
    NSDictionary *dict = @{
                           @"ent_lan"      : @"0",
                           @"ent_user_type":@"1"
                           };
    NetworkHandler *handler = [NetworkHandler sharedInstance];
    [handler composeRequestWithMethod:@"getCancellationReson" paramas:dict
                         onComplition:^(BOOL succeeded, NSDictionary *response) {
                             if (succeeded) {
                                 _cancelReasons = [response[@"data"] mutableCopy];
                              //
                                 [self.cancelTableView reloadData];
                                 self.heightOfContent.constant = self.cancelTableView.contentSize.height;
                                 [self layoutIfNeeded];
                                 [[ProgressIndicator sharedInstance] hideProgressIndicator];
                                
                                   _cancelTableView.tintColor = UIColorFromRGB(0X008000);
                             }
                         }];
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
{
    if ([touch.view isDescendantOfView:_contentView]) {
       // [_cancelTableView setEditing:YES animated:YES];
        // Don't let selections of auto-complete entries fire the
        // gesture recognizer
        return NO;
    }
    
    return YES;
}


-(void)dimissView{
    self.contentView.alpha = 1;
    
    [UIView animateWithDuration:0.3
                     animations:^{
                         self.contentView.alpha = 0.3;
                     }
                     completion:^(BOOL finished) {
                         [self removeFromSuperview];
                     }];
    
}

- (IBAction)submitTheRating:(id)sender{
    if (_selectedIndex == -1) {
        [Helper showAlertWithTitle:NSLocalizedString(@"Message",@"Message") Message:NSLocalizedString(@"Please Select Any Reason",@"Please Select Any Reason")];
    }
    else{
    self.contentView.alpha = 1;
    
    [UIView animateWithDuration:0.3
                     animations:^{
                         self.contentView.alpha = 0.3;
                     }
                     completion:^(BOOL finished) {
                         [_delegate popUpRatingOFfDismiss:_cancelReasons[_selectedIndex]];
                         [self removeFromSuperview];
                     }];
    }
}

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [[self.cancelTableView cellForRowAtIndexPath:indexPath] setAccessoryType:UITableViewCellAccessoryNone];
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _cancelReasons.count;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
       tableView.allowsMultipleSelection = false;
    if (_selectedIndex == indexPath.row) {
        _selectedIndex = -1;
    }else{
        _selectedIndex = indexPath.row;
    }
    
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    
    if (cell.accessoryType == UITableViewCellAccessoryNone) {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
        
    } else {
            cell.accessoryType = UITableViewCellAccessoryNone;
        }
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *kCellID = @"cancel";
    CancelReasonTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kCellID];
    cell.reasonLabel.text = _cancelReasons[indexPath.row];
   cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}


- (NSIndexPath*)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath*)indexPath {
    for ( NSIndexPath* selectedIndexPath in tableView.indexPathsForSelectedRows ) {
        
        if ( selectedIndexPath.section != indexPath.section ){
            [tableView deselectRowAtIndexPath:selectedIndexPath animated:NO] ;
        }
    }
    return indexPath ;
}
-(UITableViewCellEditingStyle)tableview :(UITableView *)tableview editingStyleFromRowAtIndex:(NSIndexPath *)indexpath{
    return 3;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (_cancelReasons.count == 3) {
        return 62;
    }else{
    return 46;
    }
}
@end
