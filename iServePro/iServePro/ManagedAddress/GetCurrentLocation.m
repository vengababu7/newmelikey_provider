
//
//  GetDirectionController.m
//  Homappy
//
//  Created by Rahul Sharma on 28/07/15.
//  Copyright (c) 2015 Rahul Sharma. All rights reserved.
//

#import "GetCurrentLocation.h"
#import "SignUpViewController.h"

@interface GetCurrentLocation ()<CLLocationManagerDelegate>
{
    GMSGeocoder *geocoder_;
    CLLocationManager *locationManager;
}
@end

@implementation GetCurrentLocation

static GetCurrentLocation *share;

+ (id)sharedInstance
{
    if (!share)
    {
        share  = [[self alloc] init];
    }
    return share;
}

/**
 *  All Directions Method
 */

/*---------------------------------------*/
#pragma mark - CLLocation Delegate Method
/*---------------------------------------*/

- (void)getLocation
{

    if ([CLLocationManager locationServicesEnabled])
    {
        if (!locationManager) {
            
            locationManager = [[CLLocationManager alloc] init];
            locationManager.delegate = self;
            locationManager.distanceFilter = kCLDistanceFilterNone;
            locationManager.desiredAccuracy = kCLLocationAccuracyBest;
            if  ([locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)])
            {
                [locationManager requestWhenInUseAuthorization];
            }
        }
        [locationManager startUpdatingLocation];
        
    }
    else {
        
//        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Location Service" message:@"Unable to find your location, Please enable location services in settings." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
//        [alertView show];
    }

}

/*
 To Get Updated lattitude & longitude
 @return nil.
 */
- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    CLLocation *location = [locations lastObject];
    NSString *latitude = [NSString stringWithFormat:@"%f",location.coordinate.latitude];
    NSString *longitude = [NSString stringWithFormat:@"%f",location.coordinate.longitude];
    
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];

        [ud setObject:latitude
               forKey:@"lats"];
        [ud setObject:longitude
               forKey:@"longs"];
        [ud synchronize];
        
        
         /*****************************************************/
        
        
        CLGeocoder *geocoder = [[CLGeocoder alloc] init] ;
        [geocoder reverseGeocodeLocation:location completionHandler:^(NSArray *placemarks, NSError *error)
         {
             if (!(error))
             {
                 CLPlacemark *placemark = [placemarks objectAtIndex:0];
                 
                 [ud setObject:[[placemark.addressDictionary valueForKey:@"FormattedAddressLines"] componentsJoinedByString:@", "] forKey:@"currentAddress"];
 
                 [ud setObject:[placemark country]
                        forKey:@"country"];
                 [ud setObject:[placemark locality]
                        forKey:@"city"];
                 [ud setObject:[placemark subLocality]
                        forKey:@"area"];
                 [ud setObject:[placemark postalCode] forKey:@"zipcode"];
                 [ud synchronize];
                 
                 
                 if (self.delegate && [self.delegate respondsToSelector:@selector(updatedAddress:)]) {
                     [self.delegate updatedAddress:[[placemark.addressDictionary valueForKey:@"FormattedAddressLines"] componentsJoinedByString:@", "]];
                 }
                 
                 NSLog(@"'Get Current Location Class'- Current Location:\nAddress: %@\nLat: %@\nLong :%@\nCity: %@\nZipCode: %@",[[placemark.addressDictionary valueForKey:@"FormattedAddressLines"] componentsJoinedByString:@", "],latitude,longitude,placemark.locality,placemark.postalCode);
             }
             else
             {
                 NSLog(@"Failed to update location : %@",error);

             }
         }];
        
        [locationManager stopUpdatingLocation];
        
    if (self.delegate && [self.delegate respondsToSelector:@selector(updatedLocation:and:)]) {
        [self.delegate updatedLocation:latitude.doubleValue and:longitude.doubleValue];
        }

}
@end
