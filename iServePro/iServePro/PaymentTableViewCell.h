//
//  PaymentTableViewCell.h
//  iServePro
//
//  Created by Rahul Sharma on 14/07/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PaymentTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *avgRatings;
@property (strong, nonatomic) IBOutlet UILabel *acceptRating;

@property (strong, nonatomic) IBOutlet UILabel *noOfbookings;
@property (strong, nonatomic) IBOutlet UILabel *earinings;
@property (strong, nonatomic) IBOutlet UILabel *openbal;
@property (strong, nonatomic) IBOutlet UILabel *closedbal;
@end
