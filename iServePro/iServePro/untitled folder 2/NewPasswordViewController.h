//
//  NewPasswordViewController.h
//  Onthewaypro
//
//  Created by Rahul Sharma on 01/11/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NewPasswordViewController : UIViewController

@property (strong, nonatomic) IBOutlet UITextField *password;
@property (strong, nonatomic) IBOutlet UITextField *conPassword;
- (IBAction)submitThePassword:(id)sender;

@property (strong, nonatomic) IBOutlet UIButton *submitButton;
@end
