//
//  ScheduleViewController.m
//  Onthewaypro
//
//  Created by Rahul Sharma on 02/11/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import "ScheduleViewController.h"
#import "ScheduleTableCell.h"
#import "User.h"
#import "LocationTracker.h"
#import "iServeSplashController.h"


@interface ScheduleViewController ()<JTCalendarDelegate,UserDelegate>
{
NSMutableDictionary *_eventsByDate;

NSDate *_todayDate;
NSDate *_minDate;
NSDate *_maxDate;

NSDate *_dateSelected;
}
@property (strong, nonatomic) NSMutableArray *dict;
@end


@implementation ScheduleViewController

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if(!self){
        return nil;
    }
    
    
    
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    _dict =[[NSMutableArray alloc]init];
    _calendarManager = [JTCalendarManager new];
    _calendarManager.delegate = self;
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy"];
    _yearLabel.text = [formatter stringFromDate:[NSDate date]];
    // Generate random events sort by date using a dateformatter for the demonstration
    // [self createRandomEvents];
    
    // Create a min and max date for limit the calendar, optional
    [self createMinAndMaxDate];
    [_calendarManager setMenuView:_headerView];
    [_calendarManager setContentView:_mainCalendarView];
    [_calendarManager setDate:_todayDate];
    
}
-(void)viewDidAppear:(BOOL)animated{
    [self getALLSlots];
}

-(void)getALLSlots
{
    NSString *month = [self getMonths];
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    NSDictionary *dict = @{
                           @"ent_sess_token":[ud objectForKey:KDAcheckUserSessionToken],
                           @"ent_dev_id":[ud objectForKey:kPMDDeviceIdKey],
                           @"ent_pro_id":[ud objectForKey:@"ProviderId"],
                           @"ent_month":month
                           };
    NetworkHandler *handler = [NetworkHandler sharedInstance];
    [handler composeRequestWithMethod:@"GetAllSlot"
                              paramas:dict
                         onComplition:^(BOOL succeeded, NSDictionary *response) {
                             if (succeeded) { //handle success response
                                 [self createRandomEvents:[response mutableCopy]];
                             }
                             else{
                                 [[ProgressIndicator sharedInstance]hideProgressIndicator];
                                 
                                 NSLog(@"Error");
                                 
                             }
                             
                         }];
}

/***************************************/
-(NSString *)getMonths
{
    NSDate *date = [NSDate date];
    NSCalendar *calendarLoc = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *components = [calendarLoc components:(NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit) fromDate:date];
    // NSInteger Day = [components day];
    NSInteger month = [components month];
    NSInteger year = [components year];
    
    //
    NSNumberFormatter * numberFormatter = [[NSNumberFormatter alloc] init];
    [numberFormatter setPaddingCharacter:@"0"];
    [numberFormatter setMinimumIntegerDigits:2];
    NSString * monthString = [numberFormatter stringFromNumber:[NSNumber numberWithInteger:month]];
    
    NSString *retMonth = [NSString stringWithFormat:@"%ld-%@",(long)year,monthString];
    return retMonth;
}


/***************************************/

#pragma mark - CalendarManager delegate

// Exemple of implementation of prepareDayView method
// Used to customize the appearance of dayView
- (void)calendar:(JTCalendarManager *)calendar prepareDayView:(JTCalendarDayView *)dayView
{
    // Today
    if([_calendarManager.dateHelper date:[NSDate date] isTheSameDayThan:dayView.date]){
        dayView.circleView.hidden = NO;
        dayView.circleView.backgroundColor = UIColorFromRGB(0X0094E7);
        dayView.dotView.backgroundColor = [UIColor whiteColor];
        dayView.textLabel.textColor = [UIColor whiteColor];
    }
    // Selected date
    else if(_dateSelected && [_calendarManager.dateHelper date:_dateSelected isTheSameDayThan:dayView.date]){
        dayView.circleView.hidden = NO;
        dayView.circleView.backgroundColor = UIColorFromRGB(0Xf6f7f8);
        dayView.dotView.backgroundColor = UIColorFromRGB(0X0094E7);
        dayView.textLabel.textColor = [UIColor blackColor];
    }
    // Other month
    else if(![_calendarManager.dateHelper date:_mainCalendarView.date isTheSameMonthThan:dayView.date]){
        dayView.circleView.hidden = YES;
        dayView.dotView.backgroundColor = UIColorFromRGB(0X0094E7);
        dayView.textLabel.textColor = [UIColor lightGrayColor];
    }
    // Another day of the current month
    else{
        dayView.circleView.hidden = YES;
        dayView.dotView.backgroundColor = UIColorFromRGB(0X0094E7);
        dayView.textLabel.textColor = [UIColor blackColor];
    }
    
    if([self haveEventForDay:dayView.date]){
        dayView.dotView.hidden = NO;
    }
    else{
        dayView.dotView.hidden = YES;
    }
}


- (void)calendar:(JTCalendarManager *)calendar didTouchDayView:(JTCalendarDayView *)dayView
{
    _dateSelected = dayView.date;
    
    // Animation for the circleView
    dayView.circleView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.1, 0.1);
    [UIView transitionWithView:dayView
                      duration:.3
                       options:0
                    animations:^{
                        dayView.circleView.transform = CGAffineTransformIdentity;
                        [_calendarManager reload];
                       [self.mainTableView reloadData];
                    } completion:nil];
    
    // Load the previous or next page if touch a day from another month
    
    if(![_calendarManager.dateHelper date:_mainCalendarView.date isTheSameMonthThan:dayView.date]){
        if([_mainCalendarView.date compare:dayView.date] == NSOrderedAscending){
            [_mainCalendarView loadNextPageWithAnimation];
        }
        else{
            [_mainCalendarView loadPreviousPageWithAnimation];
        }
    }
}


#pragma mark - CalendarManager delegate - Page mangement

// Used to limit the date for the calendar, optional
- (BOOL)calendar:(JTCalendarManager *)calendar canDisplayPageWithDate:(NSDate *)date
{
    return [_calendarManager.dateHelper date:date isEqualOrAfter:_minDate andEqualOrBefore:_maxDate];
}

- (void)calendarDidLoadNextPage:(JTCalendarManager *)calendar
{
    //    NSLog(@"Next page loaded");
}

- (void)calendarDidLoadPreviousPage:(JTCalendarManager *)calendar
{
    //    NSLog(@"Previous page loaded");
}

#pragma mark - Fake data

- (void)createMinAndMaxDate
{
    _todayDate = [NSDate date];
    
    // Min date will be 2 month before today
    _minDate = [_calendarManager.dateHelper addToDate:_todayDate months:-9];
    
    // Max date will be 2 month after today
    _maxDate = [_calendarManager.dateHelper addToDate:_todayDate months:2];
}

// Used only to have a key for _eventsByDate
- (NSDateFormatter *)dateFormatter
{
    static NSDateFormatter *dateFormatter;
    if(!dateFormatter){
        dateFormatter = [NSDateFormatter new];
        dateFormatter.dateFormat = @"yyyy-MM-dd";//dd-MM-yyyy
    }
    return dateFormatter;
}

- (BOOL)haveEventForDay:(NSDate *)date
{
    NSString *key = [[self dateFormatter] stringFromDate:date];
    
    if(_eventsByDate[key] && [_eventsByDate[key] count] > 0){
        return YES;
    }
    return NO;
    
}

- (void)createRandomEvents:(NSMutableDictionary *)response
{
    if ([response[@"errFlag"] integerValue]==1) {
        if ([response[@"errNum"] integerValue] == 7) {
            [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:response[@"errMsg"]];
            User *logout = [User sharedInstance];
            logout.delegate = self;
            [logout logout];
        }
        else if ([response[@"errNum"] integerValue] == 96 || [response[@"errNum"] integerValue] == 94) {
            [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:response[@"errMsg"]];
            User *logout = [User sharedInstance];
            logout.delegate = self;
            [logout logout];
        }
        else {
            [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:response[@"errMsg"]];
        }
        
    }else{
        
        _eventsByDate = [NSMutableDictionary new];
        NSMutableDictionary *apArr = [[NSMutableDictionary alloc]init];
        
        NSMutableArray *appointmentsArr = [response objectForKey:@"slots"];
        for(int i = 0; i < appointmentsArr.count; ++i){
            
            apArr = [appointmentsArr objectAtIndex:i];
            
            NSArray *appDetailArr = [apArr objectForKey:@"slot"];
            
            NSString *key = [apArr objectForKey:@"date"];
            
            if(!_eventsByDate[key]){
                _eventsByDate[key] =appDetailArr;
                
            }
        }
        [_calendarManager reload];
        [self.mainTableView reloadData];
    }
}

/*-------------------------*/
#pragma mark - UserDelegate
/*-------------------------*/
-(void)userDidLogoutSucessfully:(BOOL)sucess {
    if (sucess){
        // Logged it out Successfully
        NSLog(@"Logged it out Successfully");
    }
    else{
        // Session is Expired
        NSLog(@"Session is Expired");
    }
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:KDAcheckUserSessionToken];
    [[NSUserDefaults standardUserDefaults] synchronize];
    ProgressIndicator *pi = [ProgressIndicator sharedInstance];
    [pi hideProgressIndicator];
    [[LocationTracker sharedInstance] stopLocationTracking];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:
                                @"Main" bundle:[NSBundle mainBundle]];
    
    iServeSplashController *splah = [storyboard instantiateViewControllerWithIdentifier:@"splash"];
    
    self.navigationController.viewControllers = [NSArray arrayWithObjects:splah, nil];
}

-(void)userDidFailedToLogout:(NSError *)error
{
    ProgressIndicator *pi = [ProgressIndicator sharedInstance];
    [pi hideProgressIndicator];
}



#pragma mark - UITableView Datasource and delegate

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 105;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd"];//2012-11-22
    NSString *date;
    if (!_dateSelected) {
        date = [formatter stringFromDate:_todayDate];
    }else{
        date = [formatter stringFromDate:_dateSelected];
    }
    _dict=[_eventsByDate[date]mutableCopy];
    return [_eventsByDate[date] count];
    
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"ScheduleCell";
    ScheduleTableCell *cell;
    
    if (!cell) {
        cell  = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    }
    cell.startTime.text =_dict[indexPath.row][@"start_dt"];
    cell.endTime.text = _dict[indexPath.row][@"end_dt"];
    cell.status.text = @"OPENED";
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {

}


- (IBAction)preCalendar:(id)sender {
    NSDate *date = [NSDate date];
    NSCalendar *calendarLoc = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *components = [calendarLoc components:(NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit) fromDate:date];
    NSInteger month = [components month];
    NSInteger year = [components year];
    NSNumberFormatter * numberFormatter = [[NSNumberFormatter alloc] init];
    [numberFormatter setPaddingCharacter:@"0"];
    [numberFormatter setMinimumIntegerDigits:2];
    NSString * monthString = [numberFormatter stringFromNumber:[NSNumber numberWithInteger:month-1]];
    
   // NSString *retMonth = [NSString stringWithFormat:@"%ld-%@",(long)year,monthString];
    
    [_mainCalendarView loadPreviousPageWithAnimation];

}


- (IBAction)postCalendar:(id)sender {
    NSDate *date = [NSDate date];
    NSCalendar *calendarLoc = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *components = [calendarLoc components:(NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit) fromDate:date];
    NSInteger month = [components month];
    NSInteger year = [components year];
    NSNumberFormatter * numberFormatter = [[NSNumberFormatter alloc] init];
    [numberFormatter setPaddingCharacter:@"0"];
    [numberFormatter setMinimumIntegerDigits:2];
    NSString * monthString = [numberFormatter stringFromNumber:[NSNumber numberWithInteger:month+1]];
    
   // NSString *retMonth = [NSString stringWithFormat:@"%ld-%@",(long)year,monthString];
    
    [_mainCalendarView loadNextPageWithAnimation];

}
- (IBAction)addSlot:(id)sender {
    [self performSegueWithIdentifier:@"AddSlotSegue" sender:nil];
}
@end
