//
//  ChatSocketIOClient.m
//  Sup
//
//  Created by Rahul Sharma on 1/8/16.
//  Copyright © 2016 3embed. All rights reserved.
//
#import "ChatSocketIOClient.h"
#import "ChatSIOClient.h"
#import "zlib.h"
#import "AFNetworkActivityIndicatorManager.h"
#import "IServeAppDelegate.h"
#import "iServeSplashController.h"
#import "HomeTabBarController.h"
#import "NewBookingController.h"
#import "IServeHomeViewController.h"
#import "AddRemindersAndEvents.h"


static ChatSocketIOClient *supSocketIOClient =  nil;

@interface ChatSocketIOClient() <ChatSIOClientDelegate,CLLocationManagerDelegate>
@property ChatSIOClient *soketIOClient;
@property (nonatomic,strong) NSArray *contactArray ;
@property int num;
@property int currentNum;
@property BOOL isNewBooking;
@property (strong, nonatomic) UIWindow *window;




@end


@implementation ChatSocketIOClient


+ (instancetype) sharedInstance
{
    if (!supSocketIOClient) {
      //  static dispatch_once_t onceToken;
       // dispatch_once(&onceToken, ^{
            supSocketIOClient = [[self alloc] init];
        dispatch_async(dispatch_get_main_queue(), ^{
            [supSocketIOClient socketIOSetup];
        });
    }
    
    
    return supSocketIOClient;
}



-(void)connectSocket {
    
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        [supSocketIOClient socketIOSetup];
    }];
    
}

-(void)disconnectSocket {
    
    [_soketIOClient disconnect];
}

-(void)socketIOSetup
{
    ChatSIOConfiguration *config = [ChatSIOConfiguration defaultConfiguration];
    self.soketIOClient = [ChatSIOClient sharedInstance];
    [self.soketIOClient setConfiguration:config];
    [self.soketIOClient setDelegate:self];
    [self.soketIOClient connect];
    self.channelsName = [[NSMutableArray alloc] init];
}


#pragma mark - SIOClientDelegate

- (void) sioClient:(ChatSIOClient *)client didConnectToHost:(NSString*)host {
    NSLog(@"ChatSIO connect to %@", host);
    
    
    if (![self.channelsName containsObject:@"LiveBooking"]) {
        [self.channelsName addObject:@"LiveBooking"];
        [self.soketIOClient subscribeToChannels:@[@"LiveBooking"]];
    }
    [self heartBeat:@"1"];
}

-(void)heartBeat:(NSString *)status{
    NSString *proID;
    if ([[NSUserDefaults standardUserDefaults] objectForKey:kNSUDoctorEmailAddressKey]) {
        proID=[[NSUserDefaults standardUserDefaults] objectForKey:kNSUDoctorEmailAddressKey];
    }else{
        proID=@"Simulator";
    }
    
    NSDictionary *message=@{
                            @"status":@"1",
                            @"email":proID
                            };
    [self.soketIOClient publishToChannel:@"ProviderStatus" message:message];
}


- (void) sioClient:(ChatSIOClient *)client didSubscribeToChannel:(NSString*)channel {
    NSLog(@"ChatSIO subscribe to %@", channel);
}

- (void) sioClient:(ChatSIOClient *)client didSendMessageToChannel:(NSString *)channel {

    NSLog(@"ChatSIO message sent to %@", channel);
}


- (NSString *)stringByRemovingControlCharacters: (NSString *)inputString
{
    NSCharacterSet *controlChars = [NSCharacterSet controlCharacterSet];
    NSRange range = [inputString rangeOfCharacterFromSet:controlChars];
    if (range.location != NSNotFound) {
        NSMutableString *mutable = [NSMutableString stringWithString:inputString];
        while (range.location != NSNotFound) {
            [mutable deleteCharactersInRange:range];
            range = [mutable rangeOfCharacterFromSet:controlChars];
        }
        return mutable;
    }
    return inputString;
}



- (void) sioClient:(ChatSIOClient *)client didRecieveMessage:(NSDictionary*)message onChannel:(NSString *)channel {
    NSLog(@"ChatSIO message recieved %@ on %@", message, channel);
    [[AFNetworkActivityIndicatorManager sharedManager] setEnabled:NO];
    NSMutableArray *bookingArray = [message mutableCopy];
    NSInteger btype = [bookingArray[0][@"btype"]integerValue];
    if ([channel isEqualToString:@"LiveBooking"]) {
        IServeAppDelegate *tmpDelegate = (IServeAppDelegate *)[[UIApplication sharedApplication] delegate];
        UIApplicationState state = [[UIApplication sharedApplication] applicationState];
        if (state == UIApplicationStateBackground || state == UIApplicationStateInactive)
        {
            if (btype == 1 || btype == 3) {
                NSMutableDictionary *notificationDict = [[NSMutableDictionary alloc] init];
                [notificationDict setObject:message forKey:@"aps"];
                UILocalNotification* localNotification = [[UILocalNotification alloc] init];
                localNotification.category  = NSLocalizedString(@"JobRequest",@"JobRequest");
                localNotification.fireDate = [NSDate dateWithTimeIntervalSinceNow:1];
                localNotification.alertBody =NSLocalizedString(@"Congratulations you got a new booking request", @"Congratulations you got a new booking request") ;
                localNotification.userInfo = notificationDict;
                localNotification.soundName = @"sound.mp3";
                localNotification.timeZone = [NSTimeZone defaultTimeZone];
                
                [[NSUserDefaults standardUserDefaults]setObject:message forKey:@"bookingInfo"];
                [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
                [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"isNewbooking"];            } else if(btype == 2){
                NSMutableDictionary *notificationDict = [[NSMutableDictionary alloc] init];
                [notificationDict setObject:message forKey:@"aps"];
                UILocalNotification* localNotification = [[UILocalNotification alloc] init];
                localNotification.category  = NSLocalizedString(@"JobRequest",@"JobRequest");
                localNotification.fireDate = [NSDate dateWithTimeIntervalSinceNow:1];
                localNotification.alertBody = NSLocalizedString(@"Congratulations you got a new booking request", @"Congratulations you got a new booking request") ;
                localNotification.userInfo = notificationDict;
                localNotification.soundName = @"sound.mp3";
                localNotification.timeZone = [NSTimeZone defaultTimeZone];
                [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
                [self remainderAdded:bookingArray[0][@"stime"] endTime:bookingArray[0][@"etime"]];
            }
            
            // [tmpDelegate openNewBookingVCWithInfo:message ];
        }else{
            if (btype == 1 || btype == 3)  {
                [tmpDelegate openNewBookingVCWithInfo:message ];
            }else if(btype == 2){
                [[NSNotificationCenter defaultCenter] postNotificationName:@"DismissNewBookingVC" object:nil];
                UINavigationController *naviVC =(UINavigationController*) self.window.rootViewController;
                
                if ([[naviVC.viewControllers lastObject] isKindOfClass:[IServeHomeViewController class]]) {
                    
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"DismissNewBookingVC" object:nil];
                }
                [Helper showAlertWithTitle:NSLocalizedString(@"Message",@"Message") Message:[NSString stringWithFormat:@"Congrats you got scheduling booking from %@", bookingArray[0][@"cname"]]];
                [self remainderAdded:bookingArray[0][@"stime"] endTime:bookingArray[0][@"etime"]];
                
            }
        }
    }
}

-(void)remainderAdded:(NSString *)startTime endTime:(NSString *)endTime{
    
    
    NSTimeInterval start = [startTime doubleValue];
    NSDate * startData = [NSDate dateWithTimeIntervalSince1970:start];
  //  NSDate *FromDate = [self convertGMTtoLocalTimeConversion:startData];
    
    NSTimeInterval end = [endTime doubleValue];
    NSDate * endDate = [NSDate dateWithTimeIntervalSince1970:end];
 //   NSDate *toDate = [self convertGMTtoLocalTimeConversion:endDate];
    
    AddRemindersAndEvents *eventObj = [AddRemindersAndEvents instance];
    [eventObj eventStore];
    [eventObj setStartingDate:startData];
    [eventObj setEndingDate:endDate];
    [eventObj updateAuthorizationStatusToAccessEventStore];
}

-(NSDate *)convertGMTtoLocalTimeConversion:(NSDate *)gmtDateStr
{
    
    NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSTimeZone *local = [NSTimeZone systemTimeZone];
    [formatter setTimeZone:local];
    NSDate *localDate = gmtDateStr;  //[formatter dateFromString:gmtDateStr]; // get the date
    NSTimeInterval timeZoneOffset = [[NSTimeZone systemTimeZone] secondsFromGMT]; // You could also use the systemTimeZone method
    NSTimeInterval localTimeInterval = [localDate timeIntervalSinceReferenceDate] + timeZoneOffset;
    NSDate *localCurrentDate = [NSDate dateWithTimeIntervalSinceReferenceDate:localTimeInterval];
    
    return localCurrentDate;
}


- (void)application:(UIApplication *)application handleActionWithIdentifier:(NSString *)identifier forLocalNotification:(UILocalNotification *)notification completionHandler:(void(^)())completionHandler {
    
    NSDictionary *dict = [notification.userInfo mutableCopy];
    
    if([identifier isEqualToString:@"Accept"])
    {
        [[NSUserDefaults standardUserDefaults] setObject:@"2" forKey:@"bookingStatusResponse"];
        [[NSUserDefaults standardUserDefaults]synchronize];
        
          [[NSNotificationCenter defaultCenter] postNotificationName:@"acceptBooking" object:dict];
        
//[[NSNotificationCenter defaultCenter] postNotificationName:@"acceptBooking" object:nil userInfo:(NSDictionary *)dict];
    }
    else if([identifier isEqualToString:@"Reject"])
    {
        [[NSUserDefaults standardUserDefaults] setObject:@"3" forKey:@"bookingStatusResponse"];
        [[NSUserDefaults standardUserDefaults]synchronize];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"rejectBooking" object:nil userInfo:(NSDictionary *)dict];
    }
    if(completionHandler != nil)    //Finally call completion handler if its not nil
        completionHandler();
}




- (void) sioClient:(ChatSIOClient *)client didDisconnectFromHost:(NSString*)host {


    
    NSLog(@"ChatSIO disconnect from %@", host);
    if (_chatDelegate && [_chatDelegate respondsToSelector:@selector(didDisconnect)]) {
        [_chatDelegate didDisconnect];
    }
}

- (void) sioClient:(ChatSIOClient *)client gotError:(NSDictionary *)errorInfo {
    NSLog(@"SIO Error : %@", errorInfo);
}


-(NSString*)encodeStringTo64:(NSString*)fromString
{
    NSData *plainData = [fromString dataUsingEncoding:NSUTF8StringEncoding];
    NSString *base64String;
    if ([plainData respondsToSelector:@selector(base64EncodedStringWithOptions:)]) {
        base64String = [plainData base64EncodedStringWithOptions:kNilOptions];  // iOS 7+
    } else {
        base64String = [plainData base64Encoding];                              // pre iOS7
    }
    
    return base64String;
}


//- (void)callUser:(NSDictionary *)data
//{
//    /* Update the online status of the user here, Sending of socket will not work without that */
//    NSDictionary *requestDict1 = @{@"msisdn" : [[NSUserDefaults standardUserDefaults] valueForKey:@"userName"],
//                                   @"Status" : @"1",
//                                   @"DateTime" : @"2016-01-02 15:07:18"};
//    
//    [self.soketIOClient publishToChannel:@"UpdateOnlineStatus" message:requestDict1];
//    NSLog(@"publish UpdateOnlineStatus =%@",requestDict1);
//    
//    /* Call the call init API here */
//    NSDictionary *requestDict = @{@"from" : [[NSUserDefaults standardUserDefaults] valueForKey:@"userName"],
//                                  @"to" : [data valueForKey:@"to"],
//                                  @"call_id" : [data valueForKey:@"call_id"],@"callType":data[@"callType"],
//                                  @"userName" : [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"Name"]]
//                                  };
//   
//    [self.soketIOClient publishToChannel:@"CallInit2" message:requestDict];
//    
//    /* Subscribe to the CallEventChannel */
//    NSLog(@"publish callEvent =%@",requestDict);
//    [self.soketIOClient subscribeToChannels:@[@"CallEvent2"]];
//    
//    ///Subscribe to CallInit as well
//    [self.soketIOClient subscribeToChannels:@[@"CallInit2"]];
//}
//
//- (void) sendEvent:(NSDictionary *)data
//{
//    
//    /* Update the online status of the user here, Sending of socket will not work without that */
//    NSDictionary *requestDict1 = @{@"msisdn" : [[NSUserDefaults standardUserDefaults] valueForKey:@"userName"],
//                                   @"Status" : @"1",
//                                   @"DateTime" : @"2016-01-02 15:07:18"};
//    
//    [self.soketIOClient publishToChannel:@"UpdateOnlineStatus" message:requestDict1];
//    
//    NSDictionary *requestDict = @{@"from" :[[NSUserDefaults standardUserDefaults] valueForKey:@"userName"],
//                                  @"to" : [data valueForKey:@"to"],
//                                  @"status" : [data valueForKey:@"status"],
//                                  @"call_id":[data valueForKey:@"call_id"]};
//    
//    NSLog(@"publish to callEvent when Accept or reject cliked = %@",requestDict);
//    [self.soketIOClient publishToChannel:@"CallEvent2" message:requestDict];
//}
//
//
///* TODO - make sure this event gets called every time the call gets end, else the busy status of the user will not get updated */
//-(void) sendCallEndEvent:(NSDictionary*)data
//{
//    /* Update the online status of the user here, Sending of socket will not work without that */
//    NSDictionary *requestDict1 = @{@"msisdn" : [[NSUserDefaults standardUserDefaults] valueForKey:@"userName"],
//                                   @"Status" : @"1",
//                                   @"DateTime" : @"2016-01-02 15:07:18"};
//    
//    [self.soketIOClient publishToChannel:@"UpdateOnlineStatus" message:requestDict1];
//    
//    /* TODO - Add call_id to this call */
//    NSDictionary *requestDict = @{@"from" :[[NSUserDefaults standardUserDefaults] valueForKey:@"userName"],
//                                  @"to" : [data valueForKey:@"to"],@"call_id":data[@"call_id"]
//                                  };
//    [self.soketIOClient publishToChannel:@"callEnd" message:requestDict];
//    NSLog(@"callEnd publish");
//}


///*compress data*/
//- (NSData *)gzipInflate:(NSData*)data
//{
//    if ([data length] == 0) return data;
//    
//    unsigned full_length = [data length];
//    unsigned half_length = [data length] / 2;
//    
//    NSMutableData *decompressed = [NSMutableData dataWithLength: full_length + half_length];
//    BOOL done = NO;
//    int status;
//    
//    z_stream strm;
//    strm.next_in = (Bytef *)[data bytes];
//    strm.avail_in = [data length];
//    strm.total_out = 0;
//    strm.zalloc = Z_NULL;
//    strm.zfree = Z_NULL;
//    
//    if (inflateInit2(&strm, (15+32)) != Z_OK) return nil;
//    while (!done)
//    {
//        // Make sure we have enough room and reset the lengths.
//        if (strm.total_out >= [decompressed length])
//            [decompressed increaseLengthBy: half_length];
//        strm.next_out = [decompressed mutableBytes] + strm.total_out;
//        strm.avail_out = [decompressed length] - strm.total_out;
//        
//        // Inflate another chunk.
//        status = inflate (&strm, Z_SYNC_FLUSH);
//        if (status == Z_STREAM_END) done = YES;
//        else if (status != Z_OK) break;
//    }
//    if (inflateEnd (&strm) != Z_OK) return nil;
//    
//    // Set real length.
//    if (done)
//    {
//        [decompressed setLength: strm.total_out];
//        return [NSData dataWithData: decompressed];
//    }
//    else return nil;
//}
//
//- (NSData *)gzipDeflate:(NSData*)data
//{
//    if ([data length] == 0) return data;
//    
//    z_stream strm;
//    
//    strm.zalloc = Z_NULL;
//    strm.zfree = Z_NULL;
//    strm.opaque = Z_NULL;
//    strm.total_out = 0;
//    strm.next_in=(Bytef *)[data bytes];
//    strm.avail_in = [data length];
//    
//    // Compresssion Levels:
//    //   Z_NO_COMPRESSION
//    //   Z_BEST_SPEED
//    //   Z_BEST_COMPRESSION
//    //   Z_DEFAULT_COMPRESSION
//   
//    if (deflateInit2(&strm,Z_BEST_COMPRESSION, Z_DEFLATED, (15+16), 8, Z_DEFAULT_STRATEGY) != Z_OK) return nil;
//    
//    NSMutableData *compressed = [NSMutableData dataWithLength:16384];  // 16K chunks for expansion
//    
//    do {
//        
//        if (strm.total_out >= [compressed length])
//            [compressed increaseLengthBy: 16384];
//        
//        strm.next_out = [compressed mutableBytes] + strm.total_out;
//        strm.avail_out = [compressed length] - strm.total_out;
//        
//        deflate(&strm, Z_FINISH);
//        
//    } while (strm.avail_out == 0);
//    
//    deflateEnd(&strm);
//    
//    [compressed setLength: strm.total_out];
//    return [NSData dataWithData:compressed];
//}
//
//
//-(void)removeGetMesgAckFromServer:(NSMutableDictionary *)msgDict{
//    //remove message Ack fromserver send status 3
//    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
//        [[ChatSocketIOClient sharedInstance] sendReadMessageStatus:@"3" fromUser:[[NSUserDefaults standardUserDefaults] objectForKey:@"userName"] toUser:msgDict[@"to"] withMessageID:msgDict[@"msgId"]];
//    }];
//    
//}
//
//-(void)PublishToGetCallStatusEvent:(NSDictionary*)data
//{
//    /* Update the online status of the user here, Sending of socket will not work without that */
////    NSDictionary *requestDict1 = @{@"msisdn" : [[NSUserDefaults standardUserDefaults] objectForKey:@"userName"],
////                                   @"Status" : @"1",
////                                   @"DateTime" : @"2016-01-02 15:07:18"};
////    
////    [self.soketIOClient publishToChannel:@"UpdateOnlineStatus" message:requestDict1];
////    
////    
////    /* TODO - We need to pass only from to this socket call*/
////    NSDictionary *requestDict = @{@"from" :[[NSUserDefaults standardUserDefaults] objectForKey:@"userName"]
////                                  };
////    [self.soketIOClient publishToChannel:@"getCallStatus" message:requestDict];
////    [self.soketIOClient subscribeToChannels:@[@"getCallStatus"]];
////    [[NSUserDefaults standardUserDefaults]setValue:@"True" forKey:@"getCallStatus"];
////    [[NSUserDefaults standardUserDefaults]synchronize];
//    
//}
//
//-(void)subscribeToCallEvent
//{
//    
//    /* Update the online status of the user here, Sending of socket will not work without that */
//    NSDictionary *requestDict1 = @{@"msisdn" : [[NSUserDefaults standardUserDefaults] valueForKey:@"userName"],
//                                   @"Status" : @"1",
//                                   @"DateTime" : @"2016-01-02 15:07:18"};
//    
//    [self.soketIOClient publishToChannel:@"UpdateOnlineStatus" message:requestDict1];
//    
//    [self.soketIOClient subscribeToChannels:@[@"CallEvent2"]];
//    
//}

@end
