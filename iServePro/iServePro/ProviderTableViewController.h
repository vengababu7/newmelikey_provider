//
//  ProviderTableViewController.h
//  
//
//  Created by Rahul Sharma on 31/03/16.
//
//

#import <UIKit/UIKit.h>


@protocol ProviderTableDelegate <NSObject>

- (void)providerSelectedTypes:(NSArray *)types;

@end

@interface ProviderTableViewController : UITableViewController

@property (weak, nonatomic) id<ProviderTableDelegate>delegate;

- (IBAction)CancelAction:(id)sender;
- (IBAction)addActiom:(id)sender;

@end
