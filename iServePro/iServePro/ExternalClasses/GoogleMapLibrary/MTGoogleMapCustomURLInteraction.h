//
//  MTGoogleMapCustomURLInteraction.h
//  MapsTest
//
//  Created by Vinay Raja on 22/02/14.
//  Copyright (c) 2014 3Embed Software Tech Pvt Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

#define GoogleMapsCustomURLScheme                   @"comgooglemaps://"

#define GoogleMapsCustomURLCallbackScheme           @"comgooglemaps-x-callback://"
#define GoogleMapsCustomURLCallbackHost             @"x-callback-url/open/"

#define GoogleMapsCustomURLCallbackURL              @"url"
#define GoogleMapsCustomURLCallbackError            @"x-error"
#define GoogleMapsCustomURLCallbackSource           @"x-source"
#define GoogleMapsCustomURLCallbackSuccess          @"x-success"

#define ShowMapKeyCenter                            @"center"    //comma separated lat,long
#define ShowMapKeyMapMode                           @"mapmode"   //standard, streetview
#define ShowMapKeyViews                             @"views"     //Satellite,traffic,transit
#define ShowMapKeyZoom                              @"zoom"      //integer


#define MapModeKeyStandard                          @"standard"
#define MapModeKeyStreetView                        @"streetview"

#define MapViewsKeySatellite                        @"satellite"
#define MapViewsKeyTraffic                          @"traffic"
#define MapViewsKeyTransit                          @"transit"

#define MapSearchQuery                              @"q"

#define DirectionsStartAddress                      @"saddr"
#define DirectionsEndAddress                        @"daddr"
#define DirectionsDirectionMode                     @"directionsmode"

#define DirectionModeKeyDriving                     @"driving"
#define DirectionModeKeyTransit                     @"transit"
#define DirectionModeKeyBicycling                   @"bicycling"
#define DirectionModeKeyWalking                     @"walking"

typedef enum  {
    MapModeStandard     = 0,
    MapModeStreetview   = 1
} ShowMapMapMode;

typedef enum  {
    MapViewsSatellite   = 1,                 //Satellite only
    //3 Satellite+Traffic
    MapViewsTraffic     = 2,                   //Traffic only
    MapViewsTransit     = 4                    //Transit only
    //5 Satellite+Transit
    //6 Traffic+Transit
    //7 Satellite+Trafic+Transit
} ShowMapViews;

typedef enum {
    MapDirectionModeDriving    = 0,
    MapDirectionModeTransit    = 1,
    MapDirectionModeBicycling  = 2,
    MapDirectionModeWalking    = 3,
} MapDirectionMode;

@interface MTGoogleMapCustomURLInteraction : NSObject

+(BOOL)isGoogleMapsAppPresent;

+(BOOL)showMapWithOptions:(NSDictionary*)options allowCallback:(BOOL)callback;

+(BOOL)searchWithOptions:(NSDictionary*)options allowCallback:(BOOL)callback;

+(BOOL)showDirections:(NSDictionary*)options allowCallback:(BOOL)callback;

@end
