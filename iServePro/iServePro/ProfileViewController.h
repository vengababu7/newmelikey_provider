//
//  ProfileViewController.h
//  iServePro
//
//  Created by Rahul Sharma on 28/03/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UploadFiles.h"
#import "RoundedImageView.h"
#import "AXRatingView.h"
#import "profile.h"

@interface ProfileViewController : UIViewController<UIActionSheetDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UITextFieldDelegate,UITextViewDelegate>
{
    BOOL textFieldEditedFlag;
}


@property (strong, nonatomic) IBOutlet UIButton *editProfileButton;

- (IBAction)editProfile:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *editImageButton;
- (IBAction)editImage:(id)sender;

@property (strong, nonatomic) IBOutlet UITextField *contentTextfield;

@property (strong, nonatomic) IBOutlet UITextField *proName;


@property (strong, nonatomic) IBOutlet UILabel *catergoies;
@property (strong, nonatomic) IBOutlet UIImageView *profileImgView;

@property (strong, nonatomic) IBOutlet UILabel *noOfReviews;
@property (strong, nonatomic)NSMutableArray *profileArray;

@property (strong, nonatomic) IBOutlet AXRatingView *ratingView;
@property (strong, nonatomic) IBOutlet UIButton *logoutButton;

@property (strong, nonatomic) UIButton *addButton;

- (IBAction)addPhotosButton:(id)sender;

- (IBAction)logoutAction:(id)sender;
-(void)getProfileData;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *heightOftable;
- (IBAction)forgotPassword:(id)sender;

@end
