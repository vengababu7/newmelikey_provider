//
//  AddressManageViewController.m
//  Uberx
//
//  Created by Rahul Sharma on 19/09/14.
//  Copyright (c) 2014 Rahul Sharma. All rights reserved.
//



#import "AddressManageViewController.h"
#import "Addresscell.h"
#import "PickAddressFromMapViewController.h"
#import "ManageAddress.h"

@interface AddressManageViewController ()
{
    NSInteger value;
    BOOL isright;
    NSInteger selectedIndex;
    CGRect screenSize;
    NSMutableArray *arrDBResult;
    TSPLDatabase *db;
    
}

@end

@implementation AddressManageViewController

@synthesize getAddress,manageAddressTableView;

#pragma mark - Initial Methods -

- (void)viewDidLoad
{
    [super viewDidLoad];
    db = [[TSPLDatabase alloc]init];
    screenSize = [[UIScreen mainScreen]bounds];
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    
    manageAddressTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.manageAddressTableView.allowsMultipleSelectionDuringEditing = NO;
    [self createNavLeftButton];
}
/*-----------------------------------*/
#pragma mark - Navigation Bar methods
/*-----------------------------------*/
-(void) createNavLeftButton
{
    UIImage *buttonImageNavNormal =[UIImage imageNamed:@"bnt_bck_normal.png"];
    UIImage *buttonImageNavHigh = [UIImage imageNamed:@"bnt_bck_pressed.png"];
    UIButton *cancelButton =  [UIButton buttonWithType:UIButtonTypeCustom];
    [cancelButton addTarget:self action:@selector(backButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    [cancelButton setFrame:CGRectMake(0.0f,0.0f,buttonImageNavHigh.size.width,buttonImageNavHigh.size.height)];
    [cancelButton setFrame:CGRectMake(0.0f,0.0f,buttonImageNavNormal.size.width,buttonImageNavNormal.size.height)];
    [cancelButton setBackgroundImage:buttonImageNavHigh forState:UIControlStateHighlighted];
    [cancelButton setImage:buttonImageNavNormal forState:UIControlStateNormal];
    UIBarButtonItem *containingcancelButton = [[UIBarButtonItem alloc]
                                               initWithCustomView:cancelButton];
    UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc]
                                       initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace
                                       target:nil action:nil];
    negativeSpacer.width = -5;// it was -6 in iOS 6  you can set this as per your preference
    [self.navigationItem setLeftBarButtonItems:[NSArray arrayWithObjects:negativeSpacer,containingcancelButton, nil] animated:NO];
}

-(void)backButtonPressed
{
    [self.navigationController popViewControllerAnimated:YES];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

-(void)viewWillAppear:(BOOL)animated
{
    
    //  arrDBResult =  [[TSPLDatabase getAllDataFromTable:@"ManageAddress"] mutableCopy];
    
    
    [self getAddressFromApi];
    if (self.isFromProviderBookingVC) {
        
        self.title = @"Select Address";
    }
    else{
        
        self.title = @"Manage Addresses";
        
    }
    
}

-(void)getAddressFromApi{
    [[ProgressIndicator sharedInstance] showPIOnView:self.view withMessage:@"loading.."];
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    NSDictionary *dict = @{
                           @"ent_sess_token":[ud objectForKey:KDAcheckUserSessionToken],
                           @"ent_dev_id":[ud objectForKey:kPMDDeviceIdKey],
                           @"ent_pro_id":[ud objectForKey:@"ProviderId"]
                           };
    NetworkHandler *handler  =[NetworkHandler sharedInstance];
    [handler composeRequestWithMethod:@"GetProviderAddress"
                              paramas:dict
                         onComplition:^(BOOL succeeded, NSDictionary *response) {
                             arrDBResult = [response[@"addlist"] mutableCopy];
                             [self.manageAddressTableView reloadData];
                             [[ProgressIndicator sharedInstance]hideProgressIndicator];
                         }];
}

#pragma mark - UITableview Delegates -

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (arrDBResult.count == 0)
    {
        manageAddressTableView.backgroundView = self.messageLabel;
        manageAddressTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        [self.messageLabel setHidden:NO];
        
        [self.messageLabel setHidden:NO];
        return 0;
    }
    else
    {
        [self.messageLabel setHidden:YES];
        return arrDBResult.count;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *tableidentifier = @"adressManageCell";
    
    Addresscell *cell = (Addresscell *)[tableView dequeueReusableCellWithIdentifier:tableidentifier];
    
    if (self.isFromProviderBookingVC)
    {
        cell.selectionStyle = UITableViewCellSelectionStyleDefault;
    }
    else
    {
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    [cell.removeAddressButton setTag:indexPath.row];
    [cell bringSubviewToFront:cell.removeAddressButton];
    
    
    // ManageAddress *manageAddress = arrDBResult[indexPath.row];
    
    if([arrDBResult[indexPath.row][@"address2"] length] > 0){
        
        [cell.addressLabel setText:[NSString stringWithFormat:@"%@, %@",arrDBResult[indexPath.row][@"address2"],arrDBResult[indexPath.row][@"address1"]]];
    }
    else{
        
        [cell.addressLabel setText:[NSString stringWithFormat:@"%@",arrDBResult[indexPath.row][@"address1"]]];
    }
    
    if([[arrDBResult[indexPath.row][@"locName"] uppercaseString] isEqualToString:@"HOME"])
    {
        cell.tagImageView.image = [UIImage imageNamed:@"home_icn"];
        cell.tagLabel.text = @"HOME";
    }
    else if ([[arrDBResult[indexPath.row][@"locName"] uppercaseString] isEqualToString:@"OFFICE"])
    {
        cell.tagImageView.image = [UIImage imageNamed:@"office__icn"];
        cell.tagLabel.text = @"OFFICE";
    }
    else
    {
        cell.tagImageView.image = [UIImage imageNamed:@"other_icn"];
        cell.tagLabel.text = @"OTHER";
    }
    
    [cell layoutIfNeeded];
    
    
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSString *tableidentifier = @"adressManageCell";
    
    Addresscell *cell = (Addresscell *)[tableView dequeueReusableCellWithIdentifier:tableidentifier];
    
    // ManageAddress *manageAddress = arrDBResult[indexPath.row];
    
    if([arrDBResult[indexPath.row][@"address2"] length] > 0){
        
        [cell.addressLabel setText:[NSString stringWithFormat:@"%@, %@",arrDBResult[indexPath.row][@"address2"],arrDBResult[indexPath.row][@"address1"]]];
    }
    else{
        
        [cell.addressLabel setText:[NSString stringWithFormat:@"%@",arrDBResult[indexPath.row][@"address1"]]];
    }
    
    float height = [self measureHeightLabel:cell.addressLabel];
    
    return 6+height+8;
}


- (CGFloat)measureHeightLabel: (UILabel *)label
{
    CGSize constrainedSize = CGSizeMake(screenSize.size.width-115  , 9999);
    NSDictionary *attributesDictionary = [NSDictionary dictionaryWithObjectsAndKeys:
                                          [UIFont fontWithName:label.font.fontName size:label.font.pointSize], NSFontAttributeName,
                                          nil];
    
    NSMutableAttributedString *string = [[NSMutableAttributedString alloc] initWithString:label.text attributes:attributesDictionary];
    
    CGRect requiredHeight = [string boundingRectWithSize:constrainedSize options:NSStringDrawingUsesLineFragmentOrigin context:nil];
    
    CGRect newFrame = label.frame;
    newFrame.size.height = requiredHeight.size.height;
    return  newFrame.size.height;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    if([arrDBResult[indexPath.row][@"address2"] length] > 0){
        
        [ud setObject:[NSString stringWithFormat:@"%@, %@",arrDBResult[indexPath.row][@"address2"],arrDBResult[indexPath.row][@"address1"]] forKey:@"selectedAddress"];
    }
    else{
        
        [ud setObject:[NSString stringWithFormat:@"%@",arrDBResult[indexPath.row][@"address1"]]forKey:@"selectedAddress"];
    }
    [ud setObject:arrDBResult[indexPath.row][@"id"] forKey:@"AddressId"];
    [ud synchronize];
    [self.navigationController popViewControllerAnimated:YES];
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
}


#pragma mark - UIButton Actions -

- (IBAction)addNewAddressButtonAction:(id)sender
{
    [self performSegueWithIdentifier:@"getAddressFromMap" sender:sender];
}


- (IBAction)removeAddressButtonAction:(id)sender
{
    UIButton *mBtn = (UIButton *)sender;
    selectedIndex = mBtn.tag;
    
    
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Alert", @"Alert")
                                                        message:NSLocalizedString(@"Are you sure you want to Delete this Address?", @"Are you sure you want to Delete this Address")
                                                       delegate:self
                                              cancelButtonTitle:NSLocalizedString(@"No", @"No")
                                              otherButtonTitles:NSLocalizedString(@"Yes", @"Yes"), nil];
    [alertView show];
    
    
    
}
/*-------------------------------*/
#pragma mark - UIAlertViewDelegate
/*-------------------------------*/
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 1)
    {
        [self deleteFromAddress];
    }
    
}

-(void)deleteFromAddress{
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    NSDictionary *dict = @{
                           @"ent_sess_token":[ud objectForKey:KDAcheckUserSessionToken],
                           @"ent_dev_id":[ud objectForKey:kPMDDeviceIdKey],
                           @"ent_pro_id":[ud objectForKey:@"ProviderId"],
                           @"ent_addressid":arrDBResult[selectedIndex][@"id"],
                           
                           };
    NetworkHandler *handler = [NetworkHandler sharedInstance];
    [handler composeRequestWithMethod:@"DeleteProviderAddress"
                              paramas:dict
                         onComplition:^(BOOL succeeded, NSDictionary *response) {
                             if (succeeded) {
                                 if (succeeded) {
                                     
                                     [self getAddressFromApi];
                                 }
                             }
                         }];
}

#pragma mark - Prepare Segue -

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([segue.identifier isEqualToString:@"toPickUpAddressFromMapVC"])
    {
        PickAddressFromMapViewController *pickUpAddressFromMapVC = [segue destinationViewController];
        pickUpAddressFromMapVC.isFromProviderBookingVC = self.isFromProviderBookingVC;
        
    }
    
}

@end

