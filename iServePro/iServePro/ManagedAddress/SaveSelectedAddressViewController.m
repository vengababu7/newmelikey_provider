//
//  SaveSelectedAddressViewController.m
//  iServe_AutoLayout
//
//  Created by Apple on 14/09/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import "SaveSelectedAddressViewController.h"
#import "AddressManageViewController.h"
#import "TSPLDatabase.h"

@interface SaveSelectedAddressViewController ()<UITextFieldDelegate,UITextViewDelegate>
{
    NSInteger selectedTagButton;
    CGRect screenSize;
}

@end

@implementation SaveSelectedAddressViewController

#pragma mark - Initial Methods -
- (void)viewDidLoad {
    
    [super viewDidLoad];
    selectedTagButton = 3;
    self.otherButton.selected = YES;
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    
    screenSize = [[UIScreen mainScreen]bounds];
    UIBarButtonItem *rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Save",@"Save") style:UIBarButtonItemStylePlain
                                                                          target:self action:@selector(popupCancelBooking)];
    self.navigationItem.rightBarButtonItem = rightBarButtonItem;
    self.navigationItem.rightBarButtonItem.tintColor =UIColorFromRGB(0x008000);
    [self createNavLeftButton];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated
{
    self.selectedAddressTextView.text = self.selectedAddressDetails[@"address"];
    [self setAddressTextFieldFrame];
}

-(void)setAddressTextFieldFrame
{
    self.selectedAddressTextViewHeightConstraint.constant = [self measureHeightTextField:self.selectedAddressTextView]+8;
}

#pragma mark - Custom Methods -

- (CGFloat)measureHeightTextField:(UITextView *)textView
{
    CGSize constrainedSize = CGSizeMake(screenSize.size.width-20 , 9999);
    NSDictionary *attributesDictionary = [NSDictionary dictionaryWithObjectsAndKeys:
                                          [UIFont fontWithName:textView.font.fontName size:textView.font.pointSize], NSFontAttributeName,
                                          nil];
    
    NSMutableAttributedString *string = [[NSMutableAttributedString alloc] initWithString:textView.text attributes:attributesDictionary];
    
    CGRect requiredHeight = [string boundingRectWithSize:constrainedSize options:NSStringDrawingUsesLineFragmentOrigin context:nil];
    
    CGRect newFrame = textView.frame;
    newFrame.size.height = requiredHeight.size.height;
    return  newFrame.size.height;
}


#pragma mark - UIBUtton Action -

- (IBAction)tagAddressButtonAction:(id)sender
{
    UIButton *tagButton = (UIButton *)sender;
    for (UIView *button in tagButton.superview.subviews)
    {
        if ([button isKindOfClass:[UIButton class]])
        {
            [(UIButton *)button setSelected:NO];
        }
    }
    tagButton.selected = YES;
    selectedTagButton = tagButton.tag;
    
}

/*-----------------------------------*/
#pragma mark - Navigation Bar methods
/*-----------------------------------*/
-(void) createNavLeftButton
{
    UIImage *buttonImageNavNormal =[UIImage imageNamed:@"bnt_bck_normal.png"];
    UIImage *buttonImageNavHigh = [UIImage imageNamed:@"bnt_bck_pressed.png"];
    UIButton *cancelButton =  [UIButton buttonWithType:UIButtonTypeCustom];
    [cancelButton addTarget:self action:@selector(backButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    [cancelButton setFrame:CGRectMake(0.0f,0.0f,buttonImageNavHigh.size.width,buttonImageNavHigh.size.height)];
    [cancelButton setFrame:CGRectMake(0.0f,0.0f,buttonImageNavNormal.size.width,buttonImageNavNormal.size.height)];
    [cancelButton setBackgroundImage:buttonImageNavHigh forState:UIControlStateHighlighted];
    [cancelButton setImage:buttonImageNavNormal forState:UIControlStateNormal];
    UIBarButtonItem *containingcancelButton = [[UIBarButtonItem alloc]
                                               initWithCustomView:cancelButton];
    UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc]
                                       initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace
                                       target:nil action:nil];
    negativeSpacer.width = -5;// it was -6 in iOS 6  you can set this as per your preference
    [self.navigationItem setLeftBarButtonItems:[NSArray arrayWithObjects:negativeSpacer,containingcancelButton, nil] animated:NO];
}
-(void)backButtonPressed
{
    [self.navigationController popViewControllerAnimated:YES];
    
    [self.view endEditing:YES];
    
}


- (void)popupCancelBooking
{
    if(self.selectedAddressTextView.text.length == 0)
    {
        [Helper showAlertWithTitle:@"Message" Message:@"Address should not be empty"];
    }
    else
    {
        [self.view endEditing:YES];
        // [self sendRequestToSaveAddress];
        [self addAddressToApi];
    }
}

#pragma mark - KeyBoard Methods -

- (void)keyboardHiding:(__unused NSNotification *)inputViewNotification
{
    UIEdgeInsets ei = UIEdgeInsetsMake(0.0, 0.0, 0.0, 0.0);
    self.scrollView.contentOffset = CGPointMake(0,0);
    
    [UIView animateWithDuration:0.2 animations:^{
        
        self.scrollView.scrollIndicatorInsets = ei;
        self.scrollView.contentInset = ei;
    }];
}

- (void)keyboardShowing:(__unused NSNotification *)inputViewNotification
{
    
    CGRect inputViewFrame = [[[inputViewNotification userInfo] valueForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    CGRect inputViewFrameInView = [self.view convertRect:inputViewFrame fromView:nil];
    CGRect intersection = CGRectIntersection(self.scrollView.frame, inputViewFrameInView);
    UIEdgeInsets ei = UIEdgeInsetsMake(0.0,0.0, intersection.size.height, 0.0);
    self.scrollView.scrollIndicatorInsets = ei;
    self.scrollView.contentInset = ei;
}

#pragma mark - UITextField Delegate methods -

- (BOOL) textFieldShouldBeginEditing:(UITextField *)textField
{
    return YES;
}

-(BOOL)textFieldShouldClear:(UITextField *)textField
{
    return YES;
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}


#pragma mark - UITextView Delegate -

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView
{
    return YES;
}

- (void)textViewDidEndEditing:(UITextView *)textView
{
    [textView resignFirstResponder];
}


- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    if([text isEqualToString:@"\n"]) {
        
        [textView resignFirstResponder];
        
        return NO;
    }
    
    return YES;
}




#pragma mark - Web Service Call -

-(void)sendRequestToSaveAddress
{
    [[ProgressIndicator sharedInstance] showPIOnWindow:[UIApplication sharedApplication].keyWindow withMessge:NSLocalizedString(@"Saving...", @"Saving...")];
    
    NSString *selectedTagAddress;
    switch (selectedTagButton) {
        case 1:
            selectedTagAddress = @"HOME";
            break;
        case 2:
            selectedTagAddress = @"OFFICE";
            break;
        case 3:
            selectedTagAddress = @"OTHER";
            break;
        default:
            selectedTagAddress = @"OTHER";
            break;
    }
    NSDictionary *dict = @{
                           @"address"     : _selectedAddressTextView.text,
                           @"flatno"      :_flatNumberTextField.text,
                           @"typeaddress" :selectedTagAddress,
                           @"latit"       :_selectedAddressDetails[@"lat"],
                           @"longit"      :_selectedAddressDetails[@"log"]
                           };
    TSPLDatabase *db =[[TSPLDatabase alloc]init];
    [db makeDataEntryForAddressTable:dict];
    NSArray *vcs = self.navigationController.viewControllers;
    
    NSInteger count = [vcs count];
    [[ProgressIndicator sharedInstance]hideProgressIndicator];
    [self.navigationController popToViewController:[vcs objectAtIndex:count-3] animated:YES];
}


-(void)addAddressToApi
{
    NSString *selectedTagAddress;
    switch (selectedTagButton) {
        case 1:
            selectedTagAddress = @"HOME";
            break;
        case 2:
            selectedTagAddress = @"OFFICE";
            break;
        case 3:
            selectedTagAddress = @"OTHER";
            break;
        default:
            selectedTagAddress = @"OTHER";
            break;
    }
    
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    NSDictionary *dict = @{
                           @"ent_sess_token":[ud objectForKey:KDAcheckUserSessionToken],
                           @"ent_dev_id":[ud objectForKey:kPMDDeviceIdKey],
                           @"ent_pro_id":[ud objectForKey:@"ProviderId"],
                           @"ent_locName":selectedTagAddress,
                           @"ent_address1":_selectedAddressTextView.text,
                           @"ent_address2":_flatNumberTextField.text,
                           @"ent_lat":_selectedAddressDetails[@"lat"],
                           @"ent_lng":_selectedAddressDetails[@"log"]
                           };
    NetworkHandler *handler = [NetworkHandler sharedInstance];
    [handler composeRequestWithMethod:@"AddProviderAddress"
                              paramas:dict
                         onComplition:^(BOOL succeeded, NSDictionary *response) {
                             if (succeeded) {
                                 NSArray *vcs = self.navigationController.viewControllers;
                                 NSInteger count = [vcs count];
                                 [[ProgressIndicator sharedInstance]hideProgressIndicator];
                                 [self.navigationController popToViewController:[vcs objectAtIndex:count-3] animated:YES];
                             }
                         }];
}

@end
