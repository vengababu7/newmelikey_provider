//
//  ReviewTableViewCell.h
//  iServePro
//
//  Created by Rahul Sharma on 22/08/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ReviewTableViewCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UIImageView *profileImages;
@property (strong, nonatomic) IBOutlet UILabel *custName;
@property (strong, nonatomic) IBOutlet UILabel *timeAgo;

@property (strong, nonatomic) IBOutlet UILabel *review;
@end
