//
//  ScheduleCollectionCell.h
//  Onthewaypro
//
//  Created by Rahul Sharma on 03/11/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ScheduleCollectionCell : UICollectionViewCell
@property (strong, nonatomic) IBOutlet UILabel *scheduledSlots;

@end
