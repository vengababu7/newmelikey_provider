    //
//  LocationServicesViewController.m
//  Roadyo
//
//  Created by Rahul Sharma on 5/12/15.
//  Copyright (c) 2015 3Embed. All rights reserved.
//

#import "LocationServicesViewController.h"

@interface LocationServicesViewController ()


@end

@implementation LocationServicesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(checkLocationServices) name:@"LocationServices" object:nil];
    self.title=@"Melikey";
    _clickHere.layer.borderWidth=2;
    _clickHere.layer.borderColor=UIColorFromRGB(0X008000).CGColor;

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)checkLocationServices {
    
    if([CLLocationManager locationServicesEnabled])
    {
        [self.navigationController dismissViewControllerAnimated:YES completion:nil];
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)navigateSetting:(id)sender
{
      [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
}
@end
