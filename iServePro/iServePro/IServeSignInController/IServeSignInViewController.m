//
//  SignInViewController.m
//  IservePro
//
//  Created by Rahul Sharma on 13/02/16.
//  Copyright (c) 2014 Rahul Sharma. All rights reserved.
//

#import "IServeSignInViewController.h"
#import "ForgotPassword.h"
#import "HomeTabBarController.h"
@interface IServeSignInViewController ()<CLLocationManagerDelegate>
@property(nonatomic,assign)double currentLatitude;
@property(nonatomic,assign)double currentLongitude;

@property(nonatomic,strong)CLLocationManager *locationManager;
@end

@implementation IServeSignInViewController

@synthesize emailTextField;
@synthesize passwordTextField;
@synthesize emailImageView;
@synthesize loginBlock;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}
-(void)getCurrentLocation{
    if ([CLLocationManager locationServicesEnabled]) {
        
        if (!_locationManager) {
            _locationManager = [[CLLocationManager alloc] init];
            _locationManager.delegate = self;
            if ([_locationManager respondsToSelector:@selector(requestAlwaysAuthorization)]) {
                
                [_locationManager requestAlwaysAuthorization];
            }
            _locationManager.distanceFilter = kCLDistanceFilterNone;
            _locationManager.desiredAccuracy = kCLLocationAccuracyBest;
            
        }
        [_locationManager startUpdatingLocation];
    }
    else {
        
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Location Service",@"Location Service") message:NSLocalizedString(@"Unable to find your location,Please enable location services.",@"Unable to find your location,Please enable location services.") delegate:nil cancelButtonTitle:NSLocalizedString(@"Ok",@"Ok") otherButtonTitles:nil, nil];
        [alertView show];
        
    }
}

#pragma mark UIViewLifeCycle -

- (void)viewDidLoad
{
    [super viewDidLoad];
 
//    if ([Helper isIphone5]) {
//        self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"login_bg-568h.png"]];
//    }else{
//        self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"login_bg.png"]];
//    }
    self.navigationController.navigationBarHidden = NO;
    self.navigationItem.hidesBackButton           = NO;
    self.navigationController.navigationBar.topItem.title =NSLocalizedString(@"Sign In",@"Sign In");
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName: UIColorFromRGB(0x008000)};
    [[UINavigationBar appearance] setTitleTextAttributes:
     [NSDictionary dictionaryWithObjectsAndKeys:
      [UIFont fontWithName:@"OpenSans-regular" size:18.0],NSFontAttributeName,
      nil]];
    [self createNavLeftButton];


    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(dismissKeyboard)];
    [self.view addGestureRecognizer:tapGesture];
    _currentLongitude = 0.0;
    _currentLatitude = 0.0;

}

-(void)viewDidAppear:(BOOL)animated{
    emailTextField.text = [[NSUserDefaults standardUserDefaults] objectForKey:@"emailORphone"];
    passwordTextField.text = [[NSUserDefaults standardUserDefaults] objectForKey:@"password"];
    if (emailTextField.text.length > 0 && passwordTextField.text.length > 0) {
        [passwordTextField becomeFirstResponder];
    }else{
         [emailTextField becomeFirstResponder];
    }
    
    self.navigationController.navigationBarHidden = NO;
    self.navigationItem.hidesBackButton           = NO;
    [self getCurrentLocation];
}

- (void)viewDidDisappear:(BOOL)animated
{
    
}


-(void)dismissKeyboard
{
    [emailTextField resignFirstResponder];
    [passwordTextField resignFirstResponder];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

-(void)signInUser
{
    NSString *email = emailTextField.text;
    NSString *password = passwordTextField.text;
    NSUserDefaults *ud =[NSUserDefaults standardUserDefaults];
    if (_isRemembered && email.length >0 && password.length > 0) {
        [ud setObject:email forKey:@"emailORphone"];
        [ud setObject:password forKey:@"password"];
        [ud synchronize];
    }
  
    if((unsigned long)email.length == 0)
    {
        [Helper showAlertWithTitle:NSLocalizedString(@"Message",@"Message") Message:NSLocalizedString(@"Enter Email ID",@"Enter Email ID")];
        [emailTextField becomeFirstResponder];
    }
    
    else if((unsigned long)password.length == 0)
    {
        [Helper showAlertWithTitle:NSLocalizedString(@"Message",@"Message") Message:NSLocalizedString(@"Enter Password",@"Enter Password")];
        [passwordTextField becomeFirstResponder];
    }
//    else if([self emailValidationCheck:email] == 0)
//    {
//        //email is not valid
//        [Helper showAlertWithTitle:@"Message" Message:@"Invalid Email ID"];
//        emailTextField.text = @"";
//        [emailTextField becomeFirstResponder];
//        
//    }
    else
    {
        [self.view endEditing:YES];
        checkLoginCredentials = YES;
        [self sendServiceForLogin];
        
    }
    
}

- (void)sendServiceForLogin
{
    
    ProgressIndicator *progressIndicator = [ProgressIndicator sharedInstance];
    [progressIndicator showPIOnView:self.view withMessage:NSLocalizedString(@"Signing in...",@"Signing in...")];
    
    NSString * pushToken;
    if([[NSUserDefaults standardUserDefaults]objectForKey:kSMPgetPushToken])
    {
        pushToken =[[NSUserDefaults standardUserDefaults]objectForKey:kSMPgetPushToken];
        
    }
    else
    {
        pushToken =@"dgfhfghr765998ghghj";
    }
    
     NSDictionary *queryParams;
    queryParams =@{
                   kSMPLoginEmail       :emailTextField.text,
                   kSMPLoginPassword    :passwordTextField.text,
                   kSMPLoginDevideId    :[[NSUserDefaults standardUserDefaults] objectForKey:kPMDDeviceIdKey],
                   kSMPLoginDeviceType  :@"1",
                   kSMPSignUpLattitude  :[NSNumber numberWithDouble:_currentLatitude],
                   kSMPSignUpLongitude  :[NSNumber numberWithDouble:_currentLongitude],
                   kSMPCommonUpDateTime :[Helper getCurrentDateTime],
                   @"ent_push_token"    :pushToken,
                   @"ent_app_version"   :@"1.0"
                   };
    
    NSLog(@" signin Parameters %@",queryParams);
    NetworkHandler *handler = [NetworkHandler sharedInstance];
    
    [handler composeRequestWithMethod:MethodPatientLogin
                              paramas:queryParams
                         onComplition:^(BOOL succeeded, NSDictionary *response) {
                             [[ProgressIndicator sharedInstance]hideProgressIndicator];
                             
                             if (succeeded) { //handle success response
                                 [self loginResponse:response];
                             }
                             else{
                                 NSLog(@"Error");
                             }
                         }];
}

- (void)loginResponse:(NSDictionary *)response{
    
    //  NSLog(@"response:%@",response);
    ProgressIndicator *pi = [ProgressIndicator sharedInstance];
    if (!response)
    {
        
        [pi hideProgressIndicator];
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error",@"Error") message:[response objectForKey:@"Message"] delegate:nil cancelButtonTitle:NSLocalizedString(@"OK",@"OK") otherButtonTitles:nil];
        [alertView show];
        
    }else if (response[@"error"]){
        ProgressIndicator *pi = [ProgressIndicator sharedInstance];
        [pi hideProgressIndicator];
        [Helper showAlertWithTitle:NSLocalizedString(@"error",@"error") Message:[response objectForKey:@"error"]];
    }
    else if ([response objectForKey:@"Error"])
    {
        ProgressIndicator *pi = [ProgressIndicator sharedInstance];
        [pi hideProgressIndicator];
        [Helper showAlertWithTitle:NSLocalizedString(@"error",@"error") Message:[response objectForKey:@"Error"]];
        
    }
    else
    {
        NSDictionary *dictResponse  = response;// =[response objectForKey:@"ItemsList"];
        if ([[dictResponse objectForKey:@"errFlag"] intValue] == 0)
        {
            NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
            [ud setObject:response[@"token"] forKey:KDAcheckUserSessionToken];
            [ud setObject:response[@"chn"] forKey:kNSURoadyoPubNubChannelkey];
            [ud setObject:response[@"subsChn"] forKey:kNSUDoctorSubscribeChanelKey];
            [ud setObject:response[@"email"] forKey:kNSUDoctorEmailAddressKey];
            [ud setObject:response[@"profilePic"] forKey:kNSUDoctorProfilePicKey];
            [ud setObject:response[@"fName"] forKey: @"dDoctorName"];
            [ud setObject:response[@"typeId"] forKey:@"doctorType"];
            [ud setObject:response[@"phone"] forKey:@"doctorPhone"];
            [ud setObject:response[@"serverChn"] forKey:kNSUDoctorServerChanelKey];
            [ud setObject:response[@"proid"] forKey:@"ProviderId"];
            [ud setObject:response[@"fees_group"] forKey:@"category"];
            [ud setObject:@"0" forKey:@"signing"];
            [ud setObject:response[@"proid"] forKey:@"proid"];
            [ud setBool:YES forKey:@"LoggedIn"];
            
            [ud setObject:self.emailTextField.text forKey:@"UserEmailID"];
            [ud setObject:self.passwordTextField.text forKey:@"UserPassword"];
            [ud setValue:@"4" forKey:@"IserveStatus"];
            [ud synchronize];
            
            if (self.loginBlock) {
                
                [self dismissViewControllerAnimated:YES completion:^{
                    self.loginBlock(YES);
                }];
            }
            else {
                UIStoryboard *storyboard = [UIStoryboard storyboardWithName:
                                            @"Main" bundle:[NSBundle mainBundle]];
                HomeTabBarController  *viewcontroller = [storyboard instantiateViewControllerWithIdentifier:@"home"];
                self.navigationController.viewControllers = [NSArray arrayWithObjects:viewcontroller, nil];
            }
        }
        else
        {
            [pi hideProgressIndicator];
            [Helper showAlertWithTitle:NSLocalizedString(@"Message",@"Message") Message:response[@"errMsg"]];
        }
    }
}

#pragma mark - TextFieldDelegate
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    
}

- (BOOL)textFieldShouldClear:(UITextField *)textField
{
    return YES;
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    
    if(textField == emailTextField)
    {
        [passwordTextField becomeFirstResponder];
    }
    else {
        
        [passwordTextField resignFirstResponder];
        [self signInButtonClicked:nil];
        
    }
    return YES;
    
}

- (IBAction)forgotPasswordButtonClicked:(id)sender
{
    [self.view endEditing:YES];
    UIStoryboard *mainstoryboard;
    
    if (!mainstoryboard) {
        mainstoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    }
    ForgotPassword  *forgotPassword = [mainstoryboard instantiateViewControllerWithIdentifier:@"password"];
    UINavigationController *navBar1=[[UINavigationController alloc]initWithRootViewController:forgotPassword];
    [self presentViewController:navBar1 animated:YES completion:nil];
    
    
  
//    
//    UIAlertView *forgotPasswordAlert = [[UIAlertView alloc]
//                                        initWithTitle:@"Can't sign in? "
//                                        message:@"Enter your email address below and we will send you password reset instruction."
//                                        delegate:self
//                                        cancelButtonTitle:@"Cancel"
//                                        otherButtonTitles:@"Submit", nil];
//    
//    UITextField *emailForgot = [[UITextField alloc]initWithFrame:CGRectMake(30, 100, 225, 30)];
//    emailForgot.delegate =self;
//    
//    emailForgot.placeholder = @"Email";
//    
//    emailImageView.frame = CGRectMake(220,110,18,18);
//    
//    [forgotPasswordAlert addSubview:emailForgot];
//    [forgotPasswordAlert addSubview:emailImageView];
//    
//    forgotPasswordAlert.alertViewStyle = UIAlertViewStylePlainTextInput;
//    
//    [forgotPasswordAlert show];
//    
//    //forgotView.hidden=NO;
//    NSLog(@"password recovery to be done here");
    
    
}

- (IBAction)signInButtonClicked:(id)sender {
//    NSInteger bounds = self.loginButton.bounds
//    UIView.animateWithDuration(1.0, delay: 0.0, usingSpringWithDamping: 0.2, initialSpringVelocity: 10, options: nil, animations: {
//        self.loginButton.bounds = CGRect(x: bounds.origin.x - 20, y: bounds.origin.y, width: bounds.size.width + 60, height: bounds.size.height)
//        self.loginButton.enabled = false
//    }, completion: nil)
    [self signInUser];
}

- (void) forgotPaswordAlertviewTextField
{
    UIAlertView *forgotPasswordAlert = [[UIAlertView alloc]
                                        initWithTitle:NSLocalizedString(@"Invalid Email ID",@"Invalid Email ID")
                                        message:NSLocalizedString(@"Reenter your email ID ",@"Reenter your email ID ")
                                        delegate:self
                                        cancelButtonTitle:NSLocalizedString(@"Cancel",@"Cancel")
                                        otherButtonTitles:NSLocalizedString(@"Submit",@"Submit"), nil];
    forgotPasswordAlert.tag = 1;
    UITextField *emailForgot = [[UITextField alloc]initWithFrame:CGRectMake(30, 100, 225, 30)];
    
    
    [forgotPasswordAlert addSubview:emailForgot];
    
    forgotPasswordAlert.alertViewStyle = UIAlertViewStylePlainTextInput;
    
    [forgotPasswordAlert show];
}

#pragma mark UIAlertView Delegate -

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    if(buttonIndex == 1)
    {
        UITextField *forgotEmailtext = [alertView textFieldAtIndex:0];
        NSLog(@"Email Name: %@", forgotEmailtext.text);
        
        if (((unsigned long)forgotEmailtext.text.length ==0) || [Helper emailValidationCheck:forgotEmailtext.text] == 0)
        {
            [self forgotPaswordAlertviewTextField];
        }
        else
        {
            [self retrievePassword:forgotEmailtext.text];
        }
    }
    else
    {
        NSLog(@"cancel");
        passwordTextField.text = @"";
    }
}

- (void)retrievePassword:(NSString *)text
{
    [[ProgressIndicator sharedInstance]showPIOnView:self.view withMessage:NSLocalizedString(@"Loading...",@"Loading...")];
    
    WebServiceHandler *handler = [[WebServiceHandler alloc] init];
    
    NSString *parameters = [NSString stringWithFormat:@"ent_email=%@&ent_user_type=%@",text,@"1"];
    
    NSString *removeSpaceFromParameter=[Helper removeWhiteSpaceFromURL:parameters];
    NSLog(@"request provider around you :%@",parameters);
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@forgotPassword",BASE_URL]];
    NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:url];
    
    [theRequest setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [theRequest setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [theRequest setHTTPMethod:@"POST"];
    [theRequest setHTTPBody:[[removeSpaceFromParameter stringByAddingPercentEscapesUsingEncoding:NSASCIIStringEncoding]
                             dataUsingEncoding:NSUTF8StringEncoding
                             allowLossyConversion:YES]];
    
    [handler placeWebserviceRequestWithString:theRequest Target:self Selector:@selector(retrievePasswordResponse:)];
    
}

-(void)retrievePasswordResponse :(NSDictionary *)response
{
    ProgressIndicator *pi = [ProgressIndicator sharedInstance];
    [pi hideProgressIndicator];
    
    NSLog(@"response:%@",response);
    if (!response)
    {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error",@"Error") message:[response objectForKey:@"Message"] delegate:nil cancelButtonTitle:NSLocalizedString(@"OK",@"OK") otherButtonTitles:nil];
        [alertView show];
        
    }
    else if ([response objectForKey:@"Error"])
    {
        [Helper showAlertWithTitle:NSLocalizedString(@"Error",@"Error") Message:[response objectForKey:@"Error"]];
        
    }
    else
    {
        NSDictionary *dictResponse=[response objectForKey:@"ItemsList"];
        if ([[dictResponse objectForKey:@"errFlag"] intValue] == 0)
        {
            [Helper showAlertWithTitle:NSLocalizedString(@"Message",@"Message") Message:[dictResponse objectForKey:@"errMsg"]];
        }
        else
        {
            [Helper showAlertWithTitle:NSLocalizedString(@"Message",@"Message") Message:[dictResponse objectForKey:@"errMsg"]];
            
        }
    }
}

- (BOOL) emailValidationCheck: (NSString *) emailToValidate{
    NSString *regexForEmailAddress = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailValidation = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",regexForEmailAddress];
    return [emailValidation evaluateWithObject:emailToValidate];
}

- (void)locationManager:(CLLocationManager *)manager
    didUpdateToLocation:(CLLocation *)newLocation
           fromLocation:(CLLocation *)oldLocation {
    
    [_locationManager stopUpdatingLocation];
    _currentLatitude = newLocation.coordinate.latitude;
    _currentLongitude = newLocation.coordinate.longitude;
    
    
}
- (void)locationManager:(CLLocationManager *)manager
       didFailWithError:(NSError *)error{
    
}

/*-----------------------------------*/
#pragma mark - Navigation Bar methods
/*-----------------------------------*/
- (void)createNavLeftButton
{
    UIButton *navCancelButton =  [UIButton buttonWithType:UIButtonTypeCustom];
    [navCancelButton setImage:[UIImage imageNamed:@"current_booking_cross_icon_off"]
                     forState:UIControlStateNormal];
    [navCancelButton setImage:[UIImage imageNamed:@"current_booking_cross_icon_on"]
                     forState:UIControlStateSelected];
    
    [navCancelButton addTarget:self action:@selector(backToController) forControlEvents:UIControlEventTouchUpInside];
    [navCancelButton setFrame:CGRectMake(10.0f,0.0f,40,40)];
    
    // Create a container bar button
    UIBarButtonItem *containingcancelButton = [[UIBarButtonItem alloc] initWithCustomView:navCancelButton];
    
    UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc]
                                       initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace
                                       target:nil action:nil];
    negativeSpacer.width = -16;// it was -6 in iOS 6
    [self.navigationItem setLeftBarButtonItems:[NSArray arrayWithObjects:negativeSpacer,containingcancelButton, nil] animated:NO];
}

-(void)backToController
{
    //[self.navigationController popViewControllerAnimated:YES];
    [self.view endEditing:YES];
   [self dismissViewControllerAnimated:YES completion:nil];
}


- (IBAction)rememberAction:(id)sender {
    NSUserDefaults *ud =[NSUserDefaults standardUserDefaults];
    if (!_rememberMe.selected) {
        _rememberMe.selected = YES;
        _isRemembered = YES;
        if (_isRemembered && emailTextField.text.length >0 && passwordTextField.text.length > 0) {
            [ud setObject:emailTextField.text forKey:@"emailORphone"];
            [ud setObject:passwordTextField.text forKey:@"password"];
            [ud synchronize];
        }
    }else{
        [ud removeObjectForKey:@"emailORphone"];
        [ud removeObjectForKey:@"password"];
         _rememberMe.selected = NO;
        _isRemembered = NO;
        
    }
}

@end
