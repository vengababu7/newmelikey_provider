//
//  BookingViewController.m
//  iServePro
//
//  Created by Rahul Sharma on 05/03/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import "BookingViewController.h"
#import "CustomSliderView.h"
#import "CustomerRatingPOPUP.h"
#import "AppointmentDetailController.h"
#import "ChatSocketIOClient.h"
#import "InvoiceViewController.h"
#import "TimerViewController.h"
#import "LocationTracker.h"
#import "MTGoogleMapCustomURLInteraction.h"
#import <Social/Social.h>
#import <MessageUI/MessageUI.h>
#import "LocationServicesViewController.h"


@interface BookingViewController()<CustomSliderViewDelegate,GMSMapViewDelegate,ratingPopDelegate,CLLocationManagerDelegate,MFMessageComposeViewControllerDelegate>

{
    GMSMapView *mapView_;
    CustomerRatingPOPUP *cancelReason;
    GMSGeocoder *geocoder_;
    NSMutableArray *waypoints_;
    NSMutableArray *waypointStrings_;
    GMSCoordinateBounds *bounds;
}
@property (strong, nonatomic) CustomSliderView *customSliderView;
@property(nonatomic,assign) NSInteger bookingStatu;
@property(nonatomic,assign) BOOL isUpdatedLocation;
@property(nonatomic,strong) GMSMarker *currentLocationMarker;
@property(nonatomic,assign) CLLocationCoordinate2D previouCoord;
@property(nonatomic,strong) CLLocationManager *locationManager;
@property(nonatomic,strong) GMSMarker *sourceMarker;
@property(nonatomic,strong) NSString *startLocation;

@end

@implementation BookingViewController


-(void)viewDidLoad
{
    [super viewDidLoad];
     _bookingStatu =[_dictBookingDetails[@"status"] integerValue];
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    self.navigationController.interactivePopGestureRecognizer.enabled = NO;
    NSString *title =[NSString stringWithFormat:@"JOB ID:%@",_dictBookingDetails[@"bid"]];
    self.title =title;
    _locationManager = [[CLLocationManager alloc] init];
    _locationManager.delegate = self;
    _locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    _locationManager.distanceFilter = kCLDistanceFilterNone;
    [_locationManager startUpdatingLocation];

    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:37.778376
                                                            longitude:-122.409853
                                                                 zoom:16];
    _mapView.camera =camera;//[GMSCameraPosition ]// mapWithFrame:CGRectMake(0, 0, 320, 236) camera:camera];
    _mapView.delegate = self;
    _mapView.settings.myLocationButton = YES;
   // _mapView.padding = UIEdgeInsetsMake(0, 0, 60, 0);
    
    _googleButton.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    _googleButton.titleLabel.textAlignment = NSTextAlignmentCenter;
    [_googleButton setTitle: NSLocalizedString(@"Navigate using\nGoogle ",@"Navigate using\nGoogle ") forState: UIControlStateNormal];
    _wazeButton.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    _wazeButton.titleLabel.textAlignment = NSTextAlignmentCenter;
    [_wazeButton setTitle: NSLocalizedString(@"Navigate using\nWaze ",@"Navigate using\nWaze ") forState: UIControlStateNormal];
    
    [self.sliderOuterView bringSubviewToFront:self.view];
    [self addCustomSlider];
    [self createNavLeftButton];
    [self updateCustomerInfo];
    [self setDropLocationOnMapWithLatitude:[_dictBookingDetails[@"apptLat"] floatValue]andLongitude:[_dictBookingDetails[@"apptLong"] floatValue]];
    if (_bookingStatu == 5) {
        UIBarButtonItem *rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Cancel",@"Cancel") style:UIBarButtonItemStylePlain
                                                                              target:self action:@selector(popupCancelBooking)];
        self.navigationItem.rightBarButtonItem = rightBarButtonItem;
        self.navigationItem.rightBarButtonItem.tintColor =UIColorFromRGB(0x008000);
    }
    
}


-(void)viewWillAppear:(BOOL)animated{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(backButtonPressed) name:@"AppointmentCancel" object:nil];

    [_mapView addObserver:self
               forKeyPath:@"myLocation"
                  options:NSKeyValueObservingOptionNew
                  context:NULL];
    dispatch_async(dispatch_get_main_queue(), ^{
        _mapView.myLocationEnabled = YES;
    });
}


- (void)viewWillDisappear:(BOOL)animated{
    
    @try {
        [_mapView removeObserver:self
                      forKeyPath:@"myLocation"
                         context:NULL];
    }
    @catch (NSException *exception) {
        
    }
    
}

-(void)popupCancelBooking{
    cancelReason = [[CustomerRatingPOPUP alloc]init];
    UIWindow *window = [[UIApplication sharedApplication] keyWindow];
    [cancelReason onWindow:window];
    cancelReason.delegate = self;
    
}
-(void)popUpRatingOFfDismiss:(NSString *)Reason{
    [self cancelAppointment:Reason];
}/**
 *  Booking Cancel Method
 *
 *  @param reason "ISERVEPRO" can be before he starts
 */

- (void)cancelAppointment:(NSString *)reason{
    
    self.navigationItem.leftBarButtonItem.enabled = NO;
    [[ProgressIndicator sharedInstance]showPIOnView:self.view withMessage:NSLocalizedString(@"Loading..",@"Loading..")];
    
    NSDictionary *parameters = @{
                                 kSMPcheckUserSessionToken: [[NSUserDefaults standardUserDefaults] objectForKey:KDAcheckUserSessionToken],
                                 kSMPCommonDevideId       :[[NSUserDefaults standardUserDefaults] objectForKey:kPMDDeviceIdKey],
//                                 @"ent_appnt_dt"          :self.dictBookingDetails[@"apntDt"],
//                                 @"ent_email"             :self.dictBookingDetails[@"email"],
                                 @"ent_reason"            :reason,
                                 @"ent_date_time"         :[Helper getCurrentDateTime],
                                 @"ent_bid":_dictBookingDetails[@"bid"]
                                 };
    
    NetworkHandler *handler = [NetworkHandler sharedInstance];
    [handler composeRequestWithMethod:MethodabortAppointment
                              paramas:parameters
                         onComplition:^(BOOL succeeded, NSDictionary *response) {
                             [[ProgressIndicator sharedInstance]hideProgressIndicator];
                             
                             if (succeeded) {
                                  [self emitCanceltoSocket:reason];
                               [self backButtonPressed];
                                 self.navigationItem.leftBarButtonItem.enabled = YES;
                             }
                             else{
                                 NSLog(@"Error");
                                 self.navigationItem.leftBarButtonItem.enabled = YES;
                             }
                         }];
}
-(void)emitCanceltoSocket:(NSString *)reason
{
    ChatSIOClient *socket = [ChatSIOClient sharedInstance];
    NSDictionary *message=@{
                            @"bid":_dictBookingDetails[@"bid"],
                            @"bstatus":[NSNumber numberWithInt:10],
                            @"cid":_dictBookingDetails[@"cid"],
                            @"proid":[[NSUserDefaults standardUserDefaults] objectForKey:@"ProviderId"],
                            @"dt":[Helper getCurrentDateTime],
                            @"ent_pro_reason":reason
                            };
    [socket publishToChannel:@"LiveBookingAck" message:message];
    
}


-(void) createNavLeftButton
{
    UIImage *buttonImageNavNormal =[UIImage imageNamed:@"bnt_bck_normal.png"];
    UIImage *buttonImageNavHigh = [UIImage imageNamed:@"bnt_bck_pressed.png"];
    UIButton *cancelButton =  [UIButton buttonWithType:UIButtonTypeCustom];
    [cancelButton addTarget:self action:@selector(backButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    [cancelButton setFrame:CGRectMake(0.0f,0.0f,buttonImageNavHigh.size.width,buttonImageNavHigh.size.height)];
    [cancelButton setFrame:CGRectMake(0.0f,0.0f,buttonImageNavNormal.size.width,buttonImageNavNormal.size.height)];
    [cancelButton setBackgroundImage:buttonImageNavHigh forState:UIControlStateHighlighted];
    [cancelButton setImage:buttonImageNavNormal forState:UIControlStateNormal];
    UIBarButtonItem *containingcancelButton = [[UIBarButtonItem alloc]
                                               initWithCustomView:cancelButton];
    UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc]
                                       initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace
                                       target:nil action:nil];
    negativeSpacer.width = -5;// it was -6 in iOS 6  you can set this as per your preference
    [self.navigationItem setLeftBarButtonItems:[NSArray arrayWithObjects:negativeSpacer,containingcancelButton, nil] animated:NO];
}
-(void)backButtonPressed {
    
    NSArray *vcs = self.navigationController.viewControllers;
    
    NSInteger count = [vcs count];
    
    if ([[vcs objectAtIndex:count-2] isKindOfClass:[AppointmentDetailController class]]) {
        
        [self.navigationController popToViewController:[vcs objectAtIndex:count-3] animated:YES];
    }else {
        [self.navigationController popViewControllerAnimated:YES];
    }
}


-(void)addCustomSlider {
    
    _customSliderView = [[[NSBundle mainBundle] loadNibNamed:@"NewSliderView" owner:self options:nil] lastObject];
    _customSliderView.delegate = self;
    _customSliderView.sliderTitle.text = NSLocalizedString(@"I HAVE ARRIVED",@"I HAVE ARRIVED");
    CGRect frame = _customSliderView.frame;
    frame.size.width = _sliderView.frame.size.width;
    frame.size.height = _sliderView.frame.size.height;
    _customSliderView.frame = frame;
    [_sliderView addSubview:_customSliderView];
    
}


#pragma mark - Custom Slider Delegate

-(void)sliderAction
{
    [self onTHEWAYButtonTapped];
}


-(void)updateCustomerInfo {
    _custName.text =_dictBookingDetails[@"fname"];
    _custAddress1.text=_dictBookingDetails[@"addrLine1"];
    _custAddress2.text=_dictBookingDetails[@"addrLine2"];
    
    switch (_bookingStatu) {
            
        case 5:
            _customSliderView.sliderTitle.text  = NSLocalizedString(@"I HAVE ARRIVED",@"I HAVE ARRIVED");
            break;
        default:
            break;
    }
}



-(void)onTHEWAYButtonTapped
{
    
    self.navigationItem.leftBarButtonItem.enabled = NO;
    
    [[ProgressIndicator sharedInstance]showPIOnView:self.view withMessage:NSLocalizedString(@"Loading..",@"Loading..")];
    
//    switch (_bookingStatu) {
//        case 2:
//        case 5:{
            _bookingStatu = 21;
//        }
//
//            break;
//        default:
//            break;
//    }
    NSDictionary *parameters = @{
                   kSMPcheckUserSessionToken: [[NSUserDefaults standardUserDefaults] objectForKey:KDAcheckUserSessionToken],
                   kSMPCommonDevideId       :[[NSUserDefaults standardUserDefaults] objectForKey:kPMDDeviceIdKey],
                   @"ent_appnt_dt"          :self.dictBookingDetails[@"apntDt"],
                   KSMPPatientEmail         :self.dictBookingDetails[@"email"],
                   kSMPRespondResponse      :[NSString stringWithFormat:@"%ld",(long)_bookingStatu],
                   @"ent_date_time"         :[Helper getCurrentDateTime],
                   @"ent_bid"                :self.dictBookingDetails[@"bid"]
                   };
    NetworkHandler *handler = [NetworkHandler sharedInstance];
    
    [handler composeRequestWithMethod:MethodupdateApptStatus
                              paramas:parameters
                         onComplition:^(BOOL succeeded, NSDictionary *response) {
                             [[ProgressIndicator sharedInstance]hideProgressIndicator];
                             [_customSliderView sliderImageOrigin];
                             if (succeeded) {
                                 if ([response[@"errFlag"] isEqualToString:@"1"]) {
                                     [Helper showAlertWithTitle:NSLocalizedString(@"Message",@"Message")Message:response[@"errMsg"]];
                                 }else{
                                     [self emitTheBookingACk:_bookingStatu];
                                     // [self updateCustomerInfo];
                                     if (_bookingStatu == 21) {
                                         [self performSegueWithIdentifier:@"timeVC" sender:_dictBookingDetails];
                                     }
                                 }
                             }
                             else
                             {
                                 NSLog(@"Error");
                                 [[ProgressIndicator sharedInstance] hideProgressIndicator];
                                 self.navigationItem.leftBarButtonItem.enabled = YES;
                             }
                             
                         }];
}
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
    if([[segue identifier] isEqualToString:@"timeVC"])
    {
        TimerViewController *details =[segue destinationViewController];
        details.dictBookingDetails=sender;
    }
    return;
}
-(void)emitTheBookingACk:(NSInteger)bstatus
{
    ChatSIOClient *socket = [ChatSIOClient sharedInstance];
    NSDictionary *message=@{
                            @"bid":_dictBookingDetails[@"bid"],
                            @"bstatus":[NSNumber numberWithInteger:bstatus],
                            @"cid":_dictBookingDetails[@"cid"],
                            @"proid":[[NSUserDefaults standardUserDefaults] objectForKey:@"ProviderId"],
                            @"dt":[Helper getCurrentDateTime]
                            };
    [socket publishToChannel:@"LiveBookingAck" message:message];
    
}




#pragma mark - Location Manager Delegate

-(void)locationManager:(CLLocationManager *)manager didUpdateHeading:(CLHeading *)newHeading {
    
    NSLog(@"UPDATE HEADING");
    if(newHeading.headingAccuracy>0)
    {
        NSLog(@"DID UPDATE HEADING");
    }
    
}

-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    _isUpdatedLocation = NO;
    if (!_isUpdatedLocation) {
    }
}

- (void)locationManager:(CLLocationManager *)manager
    didUpdateToLocation:(CLLocation *)newLocation
           fromLocation:(CLLocation *)oldLocation {
    
    
    if (!_isUpdatedLocation)
    {
        _isUpdatedLocation = YES;
        [self setStartLocationCoordinates:newLocation.coordinate.latitude Longitude:newLocation.coordinate.longitude];
        
    }
    
}

-(void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error {
    NSLog(@"locationManager failed to update location : %@",[error localizedDescription]);
}



#pragma mark- GMSMapviewDelegate


- (void)mapView:(GMSMapView *)mapView didTapAtCoordinate:(CLLocationCoordinate2D)coordinate
{
    
}
- (void) mapView:(GMSMapView *) mapView willMove:(BOOL)gesture{
    
    NSLog(@"willMove");
    
}

- (void) mapView:(GMSMapView *) mapView didChangeCameraPosition:(GMSCameraPosition *) position{
    
}

- (void)mapView:(GMSMapView *)mapView idleAtCameraPosition:(GMSCameraPosition *)position{
    
    NSLog(@"idleAtCameraPosition");
    
    
}
- (void)mapView:(GMSMapView *)mapView didTapInfoWindowOfMarker:(GMSMarker *)marker{
    
    [self performSegueWithIdentifier:@"doctorDetails" sender:marker];
}

- (UIView *)mapView:(GMSMapView *)mapView markerInfoWindow:(GMSMarker *)marker {
    
    NSLog(@"marker userData %@",marker.userData);
    return nil;
    
}
- (void)mapView:(GMSMapView *)mapView didBeginDraggingMarker:(GMSMarker *)marker{
    NSLog(@"didBeginDraggingMarker");
}
- (void)mapView:(GMSMapView *)mapView didEndDraggingMarker:(GMSMarker *)marker{
    NSLog(@"didEndDraggingMarker");
}

- (void)observeValueForKeyPath:(NSString *)keyPath
                      ofObject:(id)object
                        change:(NSDictionary *)change
                       context:(void *)context {
    
    if (!_isUpdatedLocation) {
        _isUpdatedLocation = YES;
        CLLocation *location = [change objectForKey:NSKeyValueChangeNewKey];
        GMSCameraUpdate *update = [GMSCameraUpdate setTarget:location.coordinate zoom:16];
        [_mapView animateWithCameraUpdate:update];
        CLLocationCoordinate2D position = CLLocationCoordinate2DMake(location.coordinate.latitude, location.coordinate.longitude);
        _currentLocationMarker.position = position;
        [CATransaction begin];
        [CATransaction setAnimationDuration:1.0];
        _currentLocationMarker.groundAnchor = CGPointMake(0.5f, 0.5f);
        if (_currentLocationMarker.flat) {
        }
        [CATransaction commit];
        _currentLocationMarker.map = _mapView;
        
        _previouCoord = position;
    }
    else {
        
        CLLocation *location = [change objectForKey:NSKeyValueChangeNewKey];
        CLLocationCoordinate2D position = CLLocationCoordinate2DMake(location.coordinate.latitude, location.coordinate.longitude);
        GMSCameraUpdate *update = [GMSCameraUpdate setTarget:location.coordinate zoom:18];
        [mapView_ animateWithCameraUpdate:update];
        [CATransaction begin];
        [CATransaction setAnimationDuration:1.0];
        _currentLocationMarker.position = position;
        _currentLocationMarker.icon = [UIImage imageNamed:@""];
        _currentLocationMarker.groundAnchor = CGPointMake(0.5f, 0.5f);
        [CATransaction commit];
        _previouCoord = position;
    }
    
}
-(void)checkLocationServices  {
    
    LocationServicesViewController *locationVC = [self.storyboard instantiateViewControllerWithIdentifier:@"locationVC"];
    UINavigationController *navigationVC  = [[UINavigationController alloc] initWithRootViewController:locationVC];
    [self.navigationController presentViewController:navigationVC animated:YES completion:nil];
    
}
- (void) locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status{
    
    
    if ([CLLocationManager locationServicesEnabled]) {
        switch ([CLLocationManager authorizationStatus]) {
            case kCLAuthorizationStatusAuthorizedAlways:
                [[NSNotificationCenter defaultCenter] postNotificationName:@"LocationServices" object:nil];
                break;
            case kCLAuthorizationStatusDenied:
                [self checkLocationServices];
                break;
            case kCLAuthorizationStatusAuthorizedWhenInUse:
                [[NSNotificationCenter defaultCenter] postNotificationName:@"LocationServices" object:nil];
                break;
            case kCLAuthorizationStatusNotDetermined:
                NSLog(@"Not Determined");
                break;
            case kCLAuthorizationStatusRestricted:
                NSLog(@"Restricted");
                break;
        }
        
    }else{
        LocationServicesViewController *locationVC = [self.storyboard instantiateViewControllerWithIdentifier:@"locationVC"];
        UINavigationController *navigationVC  = [[UINavigationController alloc] initWithRootViewController:locationVC];
        [self.navigationController presentViewController:navigationVC animated:YES completion:nil];
    }
}

-(void)setStartLocationCoordinates:(float)latitude Longitude:(float)longitude{
    //change map camera postion to current location
    
    _isUpdatedLocation = YES;
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:latitude
                                                            longitude:longitude
                                                                 zoom:16];
    [_mapView setCamera:camera];
    
    
    //add marker at current location
    CLLocationCoordinate2D position = CLLocationCoordinate2DMake(latitude, longitude);
    if (!_currentLocationMarker) {
        _currentLocationMarker = [[GMSMarker alloc] init];
    }
    
    _currentLocationMarker.map = _mapView;
    _currentLocationMarker.position = position;
    
}

- (IBAction)callAction:(id)sender {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Call", @"Call") message:[NSString stringWithFormat:@"%@ ",_dictBookingDetails[@"phone"]] delegate:self cancelButtonTitle:NSLocalizedString(@"Cancel", @"Cancel") otherButtonTitles:NSLocalizedString(@"Call", @"Call"), nil];
    [alert show];

}
-(void)alertView:(UIAlertView *)alertVie didDismissWithButtonIndex:(NSInteger) buttonIndex
{
    if (buttonIndex == 0)
    {
        NSLog(@"Cancel Tapped.");
    }
    else if (buttonIndex == 1)
    {
        NSString *phoneNumber = _dictBookingDetails[@"phone"];
        NSString *phoneURLString = [NSString stringWithFormat:@"tel:%@", phoneNumber];
        NSURL *phoneURL = [NSURL URLWithString:phoneURLString];
        [[UIApplication sharedApplication] openURL:phoneURL];
    }
}

- (IBAction)goToGooleMaps:(id)sender {
    LocationTracker *locaitontraker = [LocationTracker sharedInstance];
    float latitude = locaitontraker.lastLocaiton.coordinate.latitude;
    float longitude = locaitontraker.lastLocaiton.coordinate.longitude;
    CLLocationCoordinate2D position = CLLocationCoordinate2DMake(latitude, longitude);
    [self getAddress:position];

}

- (IBAction)goToWazeMaps:(id)sender {
    NSString * Lat;
    NSString * Long;
    
    CLLocationCoordinate2D position = CLLocationCoordinate2DMake([Lat doubleValue], [Long doubleValue]);
    [self getAddress:position];
    if ([[UIApplication sharedApplication]
         canOpenURL:[NSURL URLWithString:@"waze://"]])
    {
        // Waze is installed. Launch Waze and start navigation
        NSString *urlStr =[NSString stringWithFormat:@"waze://?ll=%f,%f&navigate=yes",
                           [Lat doubleValue],[Long doubleValue]];
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:urlStr]];
    }
    else
    {
        // Waze is not installed. Launch AppStore to install Waze app
        [[UIApplication sharedApplication] openURL:[NSURL
                                                    URLWithString:@"http://itunes.apple.com/us/app/id323229106"]];
    }
 
}
- (IBAction)messageAction:(id)sender {
    if ([MFMessageComposeViewController canSendText]) {
        MFMessageComposeViewController *message = [[MFMessageComposeViewController alloc] init];
        message.messageComposeDelegate = self;
         message.recipients=@[_dictBookingDetails[@"phone"]];
        [[message navigationBar] setTintColor:[UIColor blackColor]];
        message.navigationBar.titleTextAttributes = [NSDictionary dictionaryWithObject:[UIColor blackColor] forKey:NSForegroundColorAttributeName];
        
        [message setBody:@""];
        [self presentViewController:message animated:YES completion:nil];
    }

}

#pragma mark UIAddress Formatter

- (void)getAddress:(CLLocationCoordinate2D)coordinate {
    
    if (!geocoder_) {
        geocoder_ = [[GMSGeocoder alloc] init];
    }
    
    
    GMSReverseGeocodeCallback handler = ^(GMSReverseGeocodeResponse *response,
                                          NSError *error) {
        if (response && response.firstResult) {
            
            GMSAddress *address = response.firstResult;
            NSString *start = [address.lines componentsJoinedByString:@","];
            
            [self performSelectorOnMainThread:@selector(openDirection:) withObject:start waitUntilDone:YES];
            
        }else {
            // NSLog(@"Could not reverse geocode point (%f,%f): %@",
            //coordinate.latitude, coordinate.longitude, error);
        }
    };
    
    [geocoder_ reverseGeocodeCoordinate:coordinate
                      completionHandler:handler];
    
}

- (void)openDirection:(NSString *)startLocation{
    
    NSString *destination =_dictBookingDetails[@"addrLine1"];
    
    [MTGoogleMapCustomURLInteraction showDirections:@{DirectionsStartAddress: startLocation,
                                                      DirectionsEndAddress: destination,
                                                      DirectionsDirectionMode: @"Driving",
                                                      ShowMapKeyZoom: @"7",
                                                      ShowMapKeyViews: @"Satellite",
                                                      ShowMapKeyMapMode: @"standard"
                                                      }
                                      allowCallback:YES];
}

-(void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result {
    
    switch (result) {
        case MessageComposeResultCancelled:
            NSLog(@"You cancelled sending message");
            break;
        case MessageComposeResultFailed:
            NSLog(@"Message failed");
            break;
        case MessageComposeResultSent:
            NSLog(@"Message sent");
            break;
            
        default:
            NSLog(@"An error occurred while composing this message");
            break;
    }
    [self dismissViewControllerAnimated:YES completion:NULL];
}

/**
 *  Set Drop Location On Map
 *
 *  @param latitude  Latitude
 *  @param longitude Longitude
 */
- (void)setDropLocationOnMapWithLatitude:(float)latitude andLongitude:(float)longitude
{
    //change map camera postion to current location
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:latitude
                                                            longitude:longitude
                                                                 zoom:16];
    [_mapView setCamera:camera];
    //add marker at current location
    CLLocationCoordinate2D position = CLLocationCoordinate2DMake(latitude, longitude);
    _sourceMarker = [GMSMarker markerWithPosition:position];
    _sourceMarker.map = _mapView;
    [waypoints_ addObject:_sourceMarker];
    
    _sourceMarker.icon = [UIImage imageNamed:@"default_marker"];
    _startLocation = [NSString stringWithFormat:@"%.6f,%.6f",latitude, longitude];
    [waypointStrings_ insertObject:_startLocation atIndex:0];
    
    bounds = [[GMSCoordinateBounds alloc] init];
    bounds = [[GMSCoordinateBounds alloc] initWithCoordinate:_sourceMarker.position
                                                  coordinate:_currentLocationMarker.position];
    GMSCameraUpdate *update = [GMSCameraUpdate fitBounds:bounds
                                          withEdgeInsets:UIEdgeInsetsMake(40, 30, 30, 20)];
    [_mapView animateWithCameraUpdate:update];
    
}


@end


