//
//  AboutViewController.m
//  iServePro
//
//  Created by Rahul Sharma on 02/05/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import "AboutViewController.h"
#import "CustomNavigationBar.h"
#import "iServeSplashController.h"
#import "LocationTracker.h"
#import "User.h"
#import "PaymentTableViewCell.h"
#import "JobTableViewCell.h"

@interface AboutViewController () <CustomNavigationBarDelegate,UserDelegate>
@property (strong, nonatomic) IBOutlet UIButton *jobLogs;
@property (strong, nonatomic) IBOutlet UIButton *paymentLogs;
@property (strong, nonatomic) NSMutableArray *pastCycle;;
@property (strong, nonatomic) NSDictionary *currentCycle;

@property (strong, nonatomic) IBOutlet UITableView *pastTableView;
@property (strong, nonatomic) IBOutlet UITableView *currentTableView;

@end

@implementation AboutViewController


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
//    _currentView.layer.borderWidth = 2;
//    _currentView.layer.borderColor = UIColorFromRGB(0XCCCCCC).CGColor;
//    _pastView.layer.borderWidth = 2;
//    _pastView.layer.borderColor = UIColorFromRGB(0XCCCCCC).CGColor;
    [super viewDidLoad];
   
}

-(void)getFinancialData{
    [[ProgressIndicator sharedInstance] showPIOnView:self.view withMessage:@"Loading..."];
    NSDictionary *dict =@{
                          @"ent_sess_token": [[NSUserDefaults standardUserDefaults] objectForKey:KDAcheckUserSessionToken],
                          @"ent_dev_id":  [[NSUserDefaults standardUserDefaults] objectForKey:kPMDDeviceIdKey],
                          @"ent_date_time":[Helper getCurrentDateTime],
                          @"ent_pro_id": [[NSUserDefaults standardUserDefaults] objectForKey:@"ProviderId"]
                          };
    NetworkHandler *handler  =[NetworkHandler sharedInstance];
    [handler composeRequestWithMethod:@"GetFinancialData"
                              paramas:dict
                         onComplition:^(BOOL succeeded, NSDictionary *response) {
                             if (succeeded) {
                                 _pastCycle = [response[@"pastCycle"] mutableCopy];
                                 _currentCycle = response[@"currentCycle"];
                                 NSLog(@"financial data %@",response);
                                 [self.currentTableView reloadData];
                                 [self.pastTableView reloadData];
                                 [[ProgressIndicator sharedInstance] hideProgressIndicator];
                                 
                             }
                         }];
}
-(void)viewWillAppear:(BOOL)animated
{
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

-(void)viewDidAppear:(BOOL)animated {
    _pastCycle = [[NSMutableArray alloc]init];
    [self getFinancialData];
    [super viewDidAppear:animated];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(accountDeactivated) name:@"accountDeactivated" object:nil];
}

-(void)viewDidDisappear:(BOOL)animated {
    
    [super viewDidDisappear:animated];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


-(void)userDidLogoutSucessfully:(BOOL)sucess {
    
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:KDAcheckUserSessionToken];
    [[NSUserDefaults standardUserDefaults] synchronize];
    ProgressIndicator *pi = [ProgressIndicator sharedInstance];
    [pi hideProgressIndicator];
    [[LocationTracker sharedInstance] stopLocationTracking];
           UIStoryboard *storyboard = [UIStoryboard storyboardWithName:
                                    @"Main" bundle:[NSBundle mainBundle]];
        
        iServeSplashController *splah = [storyboard instantiateViewControllerWithIdentifier:@"splash"];
        
        self.navigationController.viewControllers = [NSArray arrayWithObjects:splah, nil];
  
}
-(void)userDidFailedToLogout:(NSError *)error {
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:KDAcheckUserSessionToken];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    
    ProgressIndicator *pi = [ProgressIndicator sharedInstance];
    [pi hideProgressIndicator];
    [[LocationTracker sharedInstance] stopLocationTracking];

        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:
                                    @"Main" bundle:[NSBundle mainBundle]];
        
        iServeSplashController *splah = [storyboard instantiateViewControllerWithIdentifier:@"splash"];
        
        self.navigationController.viewControllers = [NSArray arrayWithObjects:splah, nil];
}

-(void)accountDeactivated {
    
    User *user = [User sharedInstance];
    user.delegate = self;
    [user logout];
}

- (IBAction)paymentLogs:(id)sender {
    CGRect frame = _mainScrollView.bounds;
    frame.origin.x = 0;
    [_mainScrollView scrollRectToVisible:frame animated:YES];
    [self.currentTableView reloadData];

}

- (IBAction)jobsLog:(id)sender {
    CGRect frame = _mainScrollView.bounds;
    frame.origin.x = self.view.frame.size.width;
    [_mainScrollView scrollRectToVisible:frame animated:YES];
    [self.pastTableView reloadData];

}
/*---------------------------------*/
#pragma mark - Scrollview Delegate
/*---------------------------------*/
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if ([scrollView isEqual:_mainScrollView]) {
        
        CGPoint offset = scrollView.contentOffset;
        float widthOfView = CGRectGetWidth(self.view.frame);
        
        // Change Leading position of Seperator/ Selector
        self.leadingContrain.constant = offset.x / 2;
         if (offset.x < widthOfView / 2) {
             
        _paymentLogs.selected = YES;
            _jobLogs.selected = NO;
        }
        else {
            _paymentLogs.selected = NO;
            _jobLogs.selected = YES;
        }
        
        CGFloat minOffsetX = 0;
        CGFloat maxOffsetX = widthOfView * 2;
        
        if (offset.x < minOffsetX) offset.x = minOffsetX;
        if (offset.x > maxOffsetX) offset.x = maxOffsetX;
        
        scrollView.contentOffset = offset;
    }
}

#pragma mark UITableView Delegate -

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView.tag == 1) {
        return 210;
    }else{
        return 143;
    }
   
    
}



- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    

}


#pragma mark UITableview DataSource -

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    return 1;

}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{

    if (tableView.tag == 1) {
        return 1;
    }else{
        return _pastCycle.count;
    }
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell;
    NSLog(@"cellForRowAtIndexPath");
    
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    [formatter setNumberStyle:NSNumberFormatterCurrencyStyle];
    [formatter setLocale:[NSLocale currentLocale]];
    
   
    
    static NSString *CellIdentifier = @"payment";
    PaymentTableViewCell *current;
    if (!current) {
        current=[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    }
    
    static NSString *CellIdentifier1 = @"past";
    JobTableViewCell *past;
    if (!past) {
        past=[tableView dequeueReusableCellWithIdentifier:CellIdentifier1];
    }
    if (tableView.tag == 1) {
        current.layer.cornerRadius = 4;
        current.layer.masksToBounds = YES;
        current.layer.borderWidth = 2;
        current.layer.borderColor = UIColorFromRGB(0XCCCCCC).CGColor;

        
        NSNumber *bal =[NSNumber numberWithFloat:[flStrForObj(_currentCycle[@"opening_balance"]) floatValue]];
        NSString *openBal = [formatter stringFromNumber:bal];
        current.openbal.text =openBal;
        
        NSNumber *bal1 =[NSNumber numberWithFloat:[flStrForObj(_currentCycle[@"closing_balance"]) floatValue]];
        NSString *closBal = [formatter stringFromNumber:bal1];
        current.closedbal.text = closBal;
        
        
        NSNumber *earn =[NSNumber numberWithFloat:[flStrForObj(_currentCycle[@"booking_earn"]) floatValue]];
        NSString *earnBal = [formatter stringFromNumber:earn];
        current.earinings.text = earnBal;
        
        current.noOfbookings.text =[NSString stringWithFormat:@"%@", _currentCycle[@"booking_cycle"]];
        current.avgRatings.text =[NSString stringWithFormat:@"%@", _currentCycle[@"rev_rate"]];
        current.acceptRating.text=[NSString stringWithFormat:@"%@", _currentCycle[@"acpt_rate"]];
        current.selectionStyle = UITableViewCellSelectionStyleNone;
        cell = current;
    }else{
        past.layer.cornerRadius = 4;
        past.layer.masksToBounds = YES;
        past.layer.borderWidth = 2;
        past.layer.borderColor = UIColorFromRGB(0XCCCCCC).CGColor;
        
        NSNumber *paid =[NSNumber numberWithFloat:[flStrForObj(_pastCycle[indexPath.row][@"pay_amount"]) floatValue]];
        NSString *paidAmt = [formatter stringFromNumber:paid];
        past.paidAmt.text = paidAmt;
        
        NSNumber *earn =[NSNumber numberWithFloat:[flStrForObj(_pastCycle[indexPath.row][@"booking_earn"]) floatValue]];
        NSString *earnBal = [formatter stringFromNumber:earn];
        past.earnings.text = earnBal;
        
        past.noofbookings.text = _pastCycle[indexPath.row][@"no_of_bookings"];
        past.startTime.text = _pastCycle[indexPath.row][@"start_date"];
        past.endDate.text = _pastCycle[indexPath.row][@"pay_date"];
        past.selectionStyle = UITableViewCellSelectionStyleNone;
        cell = past;
    }
    
    return cell;
}

@end
