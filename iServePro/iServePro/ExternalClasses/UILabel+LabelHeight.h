//
//  UILabel+LabelHeight.h
//  iServePro
//
//  Created by Rahul Sharma on 10/03/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UILabel (LabelHeight)

+ (float)getLabelHeight:(UILabel *)label;

@end
