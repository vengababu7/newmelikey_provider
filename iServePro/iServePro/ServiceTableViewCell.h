//
//  ServiceTableViewCell.h
//  Onthewaypro
//
//  Created by Rahul Sharma on 27/10/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ServiceTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *serviceName;

@property (strong, nonatomic) IBOutlet UILabel *serviceVal;
@end
