//
//  main.m
//  iServePro
//
//  Created by Rahul Sharma on 24/02/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IServeAppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([IServeAppDelegate class]));
    }
}
