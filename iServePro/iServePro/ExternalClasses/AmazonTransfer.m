//
//  AmazonTransfer.m
//  
//
//  Created by rahul Sharma on 04/09/14.
//  Copyright (c) 2014 3Embed. All rights reserved.
//

#import "AmazonTransfer.h"


static AmazonTransfer *shared = nil;

@implementation AmazonTransfer

+(instancetype)sharedInstance
{
    if (!shared) {
        
        static dispatch_once_t onceToken;
        dispatch_once(&onceToken, ^{
            shared = [[self alloc] init];
        });
    }
    
    return shared;
}


/*
 List of AmazonRegion
 
 AWSRegionUnknown,
 AWSRegionUSEast1,
 AWSRegionUSWest1,
 AWSRegionUSWest2,
 AWSRegionEUWest1,
 AWSRegionEUCentral1,
 AWSRegionAPSoutheast1,
 AWSRegionAPNortheast1,
 AWSRegionAPSoutheast2,
 AWSRegionSAEast1,
 AWSRegionCNNorth1,

 */

+ (void) setConfigurationWithRegion:(AWSRegionType)regionType
                         accessKey:(NSString*)accessKey
                         secretKey:(NSString*)secretKey
{
    AWSStaticCredentialsProvider *credentialsProvider = [[AWSStaticCredentialsProvider alloc]initWithAccessKey:accessKey
                                                                                                     secretKey:secretKey];
    
    AWSServiceConfiguration *configuration = [[AWSServiceConfiguration alloc] initWithRegion:regionType
                                                                         credentialsProvider:credentialsProvider];;
    

    [AWSServiceManager defaultServiceManager].defaultServiceConfiguration = configuration;
}
+ (void) upload:(NSString *) localFilePath
        fileKey:(NSString *) fileKey
       toBucket:(NSString *) bucket
       mimeType:(NSString *) mimeType
completionBlock:(AWSS3TransferUtilityUploadCompletionHandlerBlock) completionBlock
{
    
    AWSS3TransferUtility *transferUtility = [AWSS3TransferUtility defaultS3TransferUtility];
    [[transferUtility uploadFile:[NSURL fileURLWithPath:localFilePath]
                          bucket:bucket
                             key:fileKey
                     contentType:mimeType
                      expression:nil
                completionHander:completionBlock]
     continueWithBlock:^id(AWSTask *task) {
         
         if (task.error) {
             
             NSLog(@"Error: %@", task.error);
         }
         
         if (task.result) {
             // Do something with uploadTask.
         }
         
         return nil;
     }];
    
    
}

+ (void) upload:(NSString *) localFilePath
         fileKey:(NSString *) fileKey
        toBucket:(NSString *) bucket
        mimeType:(NSString *) mimeType
   progressBlock:(AmazonTransferProgressBlock) progressBlock
 completionBlock:(AmazonTransferCompletionBlock) completionBlock {
    
    AWSS3TransferManagerUploadRequest *uploadRequest = [AWSS3TransferManagerUploadRequest new];
    uploadRequest.body = [NSURL fileURLWithPath:localFilePath];
    
    
    uploadRequest.key = fileKey;
    uploadRequest.bucket = bucket;
    uploadRequest.contentType = mimeType;
    uploadRequest.ACL = AWSS3BucketCannedACLPublicRead;
    
    if (progressBlock) {

        [uploadRequest setUploadProgress:(^(int64_t bytesSent, int64_t totalBytesSent, int64_t totalBytesExpectedToSend) {
            dispatch_async(dispatch_get_main_queue(), ^{
                progressBlock (totalBytesSent, totalBytesExpectedToSend);
            });
            
        })];

    }

    AWSS3TransferManager *transferManager = [AWSS3TransferManager defaultS3TransferManager];
    
    [[transferManager upload:uploadRequest]
            continueWithBlock:^id(AWSTask *task) {
                
        if (task.error) {
            if ([task.error.domain isEqualToString:AWSS3TransferManagerErrorDomain]) {
                switch (task.error.code) {
                    case AWSS3TransferManagerErrorCancelled:
                    case AWSS3TransferManagerErrorPaused:
                    {
                    }
                        break;
                    default:
                        break;
                }
            }
            
            if (completionBlock) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    completionBlock (NO, task.result, task.error);
                });
                
            }

        }
        
        if (task.result) {
            dispatch_async(dispatch_get_main_queue(), ^{
                completionBlock (YES, task.result, task.error);
            });
        }
        
        return nil;
    }];

}

+ (void) download:(NSString *) fileKey
       fromBucket:(NSString *) bucket
           toFile:(NSString *) localFilePath
    progressBlock:(AmazonTransferProgressBlock) progressBlock
  completionBlock:(AmazonTransferCompletionBlock) completionBlock {

    AWSS3TransferManagerDownloadRequest *downloadRequest = [AWSS3TransferManagerDownloadRequest new];
    downloadRequest.bucket = bucket;
    downloadRequest.key = fileKey;
    downloadRequest.downloadingFileURL = [NSURL fileURLWithPath:localFilePath];
    
    if (progressBlock) {
        downloadRequest.downloadProgress = (^(int64_t bytesWritten, int64_t totalBytesWritten, int64_t totalBytesExpectedToWrite) {
            dispatch_async(dispatch_get_main_queue(), ^{
                progressBlock (totalBytesWritten, totalBytesExpectedToWrite);
            });

        });
    }
    
    AWSS3TransferManager *transferManager = [AWSS3TransferManager defaultS3TransferManager];

    [[transferManager download:downloadRequest] continueWithBlock:^id(AWSTask *task) {
    
        if (task.error) {
            if ([task.error.domain isEqualToString:AWSS3TransferManagerErrorDomain]) {
                switch (task.error.code) {
                    case AWSS3TransferManagerErrorCancelled:
                    case AWSS3TransferManagerErrorPaused:
                    {
                    }
                        break;
                    default:
                        break;
                }
            }
            
            if (completionBlock) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    completionBlock (NO, task.result, task.error);
                });
                
            }
            
        }
        
        if (task.result) {
            dispatch_async(dispatch_get_main_queue(), ^{
                completionBlock (YES, task.result, task.error);
            });
        }
        
        return nil;

    
    }];
    
}


@end
