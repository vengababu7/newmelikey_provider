//
//  DetailsScheduleViewController.m
//  Onthewaypro
//
//  Created by Rahul Sharma on 02/11/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import "DetailsScheduleViewController.h"
#import "ScheduleCollectionCell.h"

@interface DetailsScheduleViewController ()
{
    BOOL fromTime;
    BOOL dateEnd;
    NSInteger repeatRnot;
    NSInteger weekDay;
}


@property (assign, nonatomic) BOOL fromDate;
@property (strong,nonatomic) NSString *radius;
@property (strong,nonatomic) NSString *price;
@property (strong,nonatomic) NSString *numSlots;
@property (strong,nonatomic) NSString *timeSlot;
@property (strong,nonatomic) NSString *slotStarts;
@property (strong,nonatomic) NSString *slotEnds;
@property (strong,nonatomic) NSString *timeStarts;

@end

@implementation DetailsScheduleViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self createNavLeftButton];
    self.title = @"ADD SLOTS";
    _slotsArray = [[NSMutableArray alloc]init];
    
    _everyDa.layer.borderWidth = 1;
    _everyDa.layer.borderColor = UIColorFromRGB(0X008000).CGColor;
    weekDay = 1;
    _repeatMode.selected = YES;
    repeatRnot = 2;
    _nonRepeat.selected = NO;
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(dismissKeyboard)];
    [self.view addGestureRecognizer:tapGesture];
    
    // Do any additional setup after loading the view.
}
-(void)dismissKeyboard{
    [self.view endEditing:YES];
}
-(void)viewDidAppear:(BOOL)animated{
    _addressLabel.text = [[NSUserDefaults standardUserDefaults] objectForKey:@"selectedAddress"];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*-----------------------------------*/
#pragma mark - Navigation Bar methods
/*-----------------------------------*/
-(void) createNavLeftButton
{
    UIImage *buttonImageNavNormal =[UIImage imageNamed:@"bnt_bck_normal.png"];
    UIImage *buttonImageNavHigh = [UIImage imageNamed:@"bnt_bck_pressed.png"];
    UIButton *cancelButton =  [UIButton buttonWithType:UIButtonTypeCustom];
    [cancelButton addTarget:self action:@selector(backButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    [cancelButton setFrame:CGRectMake(0.0f,0.0f,buttonImageNavHigh.size.width,buttonImageNavHigh.size.height)];
    [cancelButton setFrame:CGRectMake(0.0f,0.0f,buttonImageNavNormal.size.width,buttonImageNavNormal.size.height)];
    [cancelButton setBackgroundImage:buttonImageNavHigh forState:UIControlStateHighlighted];
    [cancelButton setImage:buttonImageNavNormal forState:UIControlStateNormal];
    UIBarButtonItem *containingcancelButton = [[UIBarButtonItem alloc]
                                               initWithCustomView:cancelButton];
    UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc]
                                       initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace
                                       target:nil action:nil];
    negativeSpacer.width = -5;// it was -6 in iOS 6  you can set this as per your preference
    [self.navigationItem setLeftBarButtonItems:[NSArray arrayWithObjects:negativeSpacer,containingcancelButton, nil] animated:NO];
}
-(void)backButtonPressed
{
    [self.navigationController popViewControllerAnimated:YES];
    [self.view endEditing:YES];
    
}


- (IBAction)goForNewAddress:(id)sender {
    
    [self performSegueWithIdentifier:@"toAddNewAddress" sender:nil];
}

- (IBAction)repeatMode:(id)sender {
    repeatRnot = 2;
    _repeatMode.selected = YES;
    _nonRepeat.selected = NO;
    [_endDateLabe setHidden:NO];
    [_endDate setHidden:NO];
    _heightOfweekDay.constant = 45;
    [self.view layoutIfNeeded];
}

- (IBAction)nonRepeat:(id)sender {
    repeatRnot = 1;
    _repeatMode.selected = NO;
    _nonRepeat.selected = YES;
    [_endDateLabe setHidden:YES];
    [_endDate setHidden:YES];
    _heightOfweekDay.constant = 0;
    [self.view layoutIfNeeded];
}

- (IBAction)startDate:(id)sender {
    dateEnd = NO;
    _fromDate = NO;
    _datePicker.datePickerMode = UIDatePickerModeDate;
    _selectType.text = @"SELECT START DATE";
    [self startAnimationUp];
}

- (IBAction)endDate:(id)sender {
    dateEnd = YES;
    _fromDate = NO;
    _datePicker.datePickerMode = UIDatePickerModeDate;
    _selectType.text = @"SELECT END DATE";
    [self startAnimationUp];
}
- (IBAction)startTime:(id)sender {
    if (!_timeSlot.length == 0 && !_numSlots.length == 0) {
        _fromDate = YES;
        _datePicker.datePickerMode = UIDatePickerModeTime;
        _selectType.text = @"SELECT START TIME";
        [self startAnimationUp];
        _slotsArray = [[NSMutableArray alloc]init];
    }else{
        [Helper showAlertWithTitle:@"Message" Message:@"Please Select The Slot Time and No of slot"];
    }
    
}

- (IBAction)cancelPicker:(id)sender {
    [self startAnimationDown];
}
- (IBAction)donePicker:(id)sender {
    
    [self startAnimationDown];
    if (_fromDate) {
        NSDateFormatter *dateFormat=[[NSDateFormatter alloc]init];
        dateFormat.dateStyle=kCFDateFormatterShortStyle;
        [dateFormat setDateFormat:@"HH:mm:ss"];//hh:mm a
        NSString *str=[NSString stringWithFormat:@"%@",[dateFormat  stringFromDate:_datePicker.date]];
        [_startTime setTitle:str forState:UIControlStateNormal];
        _timeStarts = str;
        [self createSlots:_datePicker.date];
    }else{
        NSDateFormatter *dateFormat=[[NSDateFormatter alloc]init];
        dateFormat.dateStyle=kCFDateFormatterShortStyle;
        [dateFormat setDateFormat:@"YYYY-MM-dd"];
        NSString *str=[NSString stringWithFormat:@"%@",[dateFormat  stringFromDate:_datePicker.date]];
        if (dateEnd) {
            [_endDate setTitle:str forState:UIControlStateNormal];
            _slotEnds = str;
        }else{
            [_startDate setTitle:str forState:UIControlStateNormal];
            _slotStarts = str;
        }
    }
}

-(void)createSlots:(NSDate *)myDate
{
    NSDate *dateEightHoursAhead;
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    double i = [_timeSlot floatValue]/60;
    double j = [_timeSlot floatValue]/60;
    for (i = j; i<=[_numSlots integerValue]*j; i+=j) {
        NSTimeInterval secondsInEightHours = i * 60 * 60;
        [formatter setDateFormat:@"HH:mm:ss"]; //hh:mm a
        
        if (dateEightHoursAhead) {
            _timeStarts =[formatter stringFromDate:dateEightHoursAhead];
        }
        dateEightHoursAhead = [myDate dateByAddingTimeInterval:secondsInEightHours];
        [_slotsArray addObject:[NSString stringWithFormat:@"%@    %@",_timeStarts,[formatter stringFromDate:dateEightHoursAhead]]];
    }
    [_slotsCollectionView reloadData];
}


-(void)startAnimationDown
{
    [UIView animateWithDuration:0.3
                          delay:0.1
                        options: UIViewAnimationOptionCurveEaseIn
                     animations:^{
                         _bottomSpace.constant = -157;
                         [self.view layoutIfNeeded];
                     }
                     completion:^(BOOL finished)
     {
         NSLog(@"Completed");
         
     }];
}

-(void)startAnimationUp
{
    
    [UIView animateWithDuration:0.3
                          delay:0.1
                        options: UIViewAnimationOptionCurveEaseOut
                     animations:^{
                         
                         _bottomSpace.constant = 1;
                         [self.view layoutIfNeeded];
                     }
                     completion:^(BOOL finished)
     {
         NSLog(@"Completed");
         
     }];
}
#pragma uicollectionView delegates

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return _slotsArray.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *cellIdentifier =@"slots";
    ScheduleCollectionCell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    cell.scheduledSlots.text = _slotsArray[indexPath.row];
    cell.scheduledSlots.layer.borderWidth = 1;
    cell.scheduledSlots.layer.borderColor = UIColorFromRGB(0XCCCCCC).CGColor;
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(66, 66);
}

- (IBAction)confirmSlot:(id)sender {
    [self addSlotApi];
}

-(void)addSlotApi{
    
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    NSDictionary *dict;
    if (repeatRnot == 1) {
        if (![ud objectForKey:@"AddressId"]) {
            [Helper showAlertWithTitle:@"Message" Message:@"Select The Lcoation"];
        }else if ([_distanceInRadius.text isEqualToString: @""]){
            [Helper showAlertWithTitle:@"Message" Message:@"select the Distance"];
        }else if ([_pricePerTime.text isEqualToString: @""]){
            [Helper showAlertWithTitle:@"Message" Message:@"Select The Price"];
        }
        else if ([_noOfslot.text isEqualToString: @""]){
            [Helper showAlertWithTitle:@"Message" Message:@"Select The No Of Slots"];
        }
        else if ([_pricePerTime.text isEqualToString: @""]){
            [Helper showAlertWithTitle:@"Message" Message:@"Select The Slot Amount"];
        }
        else if(_slotStarts.length == 0){
            [Helper showAlertWithTitle:@"Message" Message:@"Select The Start Date"];
        }
        else if(_timeStarts.length == 0){
            [Helper showAlertWithTitle:@"Message" Message:@"Select The Start Time"];
        }else{
            
            dict = @{
                     @"ent_sess_token":[ud objectForKey:KDAcheckUserSessionToken],
                     @"ent_dev_id":[ud objectForKey:kPMDDeviceIdKey],
                     @"ent_pro_id":[ud objectForKey:@"ProviderId"],
                     @"ent_location":[ud objectForKey:@"AddressId"],
                     @"ent_start_time":[NSString stringWithFormat:@"%@ %@",_startDate.titleLabel.text,_startTime.titleLabel.text],
                     @"ent_radius":_radius,
                     @"ent_price":_price,
                     @"ent_duration":_timeSlot,
                     @"ent_num_slot":_noOfslot.text,
                     @"ent_option":[NSString stringWithFormat:@"%d",repeatRnot]
                     };
            NetworkHandler *handler = [NetworkHandler sharedInstance];
            [handler composeRequestWithMethod:@"addslot"
                                      paramas:dict
                                 onComplition:^(BOOL succeeded, NSDictionary *response) {
                                     if (succeeded) {
                                         if ([response[@"errFlag"] integerValue]==0) {
                                             [ud removeObjectForKey:@"AddressId"];
                                             [Helper showAlertWithTitle:@"Message" Message:@"Slot Added"];
                                             [self.navigationController popViewControllerAnimated:YES];
                                             [self.view endEditing:YES];
                                         }else{
                                             [Helper showAlertWithTitle:@"Message" Message:response[@"errMsg"]];
                                         }
                                     }
                                 }];
        }
    }else{
        if (![ud objectForKey:@"AddressId"]) {
            [Helper showAlertWithTitle:@"Message" Message:@"Select The Lcoation"];
        }else if ([_distanceInRadius.text isEqualToString: @""]){
            [Helper showAlertWithTitle:@"Message" Message:@"select the Distance"];
        }else if ([_pricePerTime.text isEqualToString: @""]){
            [Helper showAlertWithTitle:@"Message" Message:@"Select The Price"];
        }
        else if ([_noOfslot.text isEqualToString: @""]){
            [Helper showAlertWithTitle:@"Message" Message:@"Select The No Of Slots"];
        }
        else if ([_pricePerTime.text isEqualToString: @""]){
            [Helper showAlertWithTitle:@"Message" Message:@"Select The Slot Amount"];
        }
        else if(_slotStarts.length == 0){
            [Helper showAlertWithTitle:@"Message" Message:@"Select The Start Date"];
        }
        else if(_timeStarts.length == 0){
            [Helper showAlertWithTitle:@"Message" Message:@"Select The Start Time"];
        }
        else if(_slotEnds.length == 0){
            [Helper showAlertWithTitle:@"Message" Message:@"Select The End Date"];
        }else{
            dict = @{
                     @"ent_sess_token":[ud objectForKey:KDAcheckUserSessionToken],
                     @"ent_dev_id":[ud objectForKey:kPMDDeviceIdKey],
                     @"ent_pro_id":[ud objectForKey:@"ProviderId"],
                     @"ent_location":[ud objectForKey:@"AddressId"],
                     @"ent_start_time":[NSString stringWithFormat:@"%@ %@",_startDate.titleLabel.text,_startTime.titleLabel.text],
                     @"ent_radius":_radius,
                     @"ent_price":_price,
                     @"ent_duration":_timeSlot,
                     @"ent_num_slot":_noOfslot.text,
                     @"ent_option":[NSString stringWithFormat:@"%d",repeatRnot],
                     @"ent_repeatday":[NSString stringWithFormat:@"%d",weekDay],
                     @"ent_end_date":[NSString stringWithFormat:@"%@",_endDate.titleLabel.text]
                     };
            NetworkHandler *handler = [NetworkHandler sharedInstance];
            [handler composeRequestWithMethod:@"addslot"
                                      paramas:dict
                                 onComplition:^(BOOL succeeded, NSDictionary *response) {
                                     if (succeeded) {
                                         if ([response[@"errFlag"] integerValue]==0) {
                                             [ud removeObjectForKey:@"AddressId"];
                                             [Helper showAlertWithTitle:@"Message" Message:@"Slot Added"];
                                             [self.navigationController popViewControllerAnimated:YES];
                                             [self.view endEditing:YES];
                                         }else{
                                             [Helper showAlertWithTitle:@"Message" Message:response[@"errMsg"]];
                                         }
                                         
                                     }
                                 }];
            
        }
    }
}

#pragma mark - TextFields

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    [formatter setNumberStyle:NSNumberFormatterCurrencyStyle];
    [formatter setLocale:[NSLocale currentLocale]];
    
    if (textField == _distanceInRadius) {
        _radius = textField.text;
        _distanceInRadius.text = [NSString stringWithFormat:@"%@ Miles",textField.text];
        
    }else  if (textField == _pricePerTime){
        _price = textField.text;
        NSNumber *amount =[NSNumber numberWithInteger:[textField.text integerValue]];
        NSString *price = [formatter stringFromNumber:amount];
        _pricePerTime.text = price;
        
    }else if (textField == _noOfslot){
        _numSlots = textField.text;
        _noOfslot.text = textField.text;
        
    }else{
        _timeSlot = textField.text;
        _slotMinute.text = [NSString stringWithFormat:@"%@ Minutes",textField.text];
        
    }
}

- (BOOL)textFieldShouldClear:(UITextField *)textField
{
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    return YES;
}

- (IBAction)everyDa:(id)sender
{
    _everyDa.layer.borderWidth = 1;
    _everyDa.layer.borderColor = UIColorFromRGB(0X008000).CGColor;
    _weekDay.layer.borderColor = UIColorFromRGB(0Xffffff).CGColor;
    _weekEnd.layer.borderColor = UIColorFromRGB(0Xffffff).CGColor;
    weekDay = 1;
}

- (IBAction)weekDay:(id)sender {
    _weekDay.layer.borderWidth = 1;
    _weekDay.layer.borderColor = UIColorFromRGB(0X008000).CGColor;
    _everyDa.layer.borderColor = UIColorFromRGB(0Xffffff).CGColor;
    _weekEnd.layer.borderColor = UIColorFromRGB(0Xffffff).CGColor;
    weekDay = 2;
}

- (IBAction)weekEnd:(id)sender {
    _weekEnd.layer.borderWidth = 1;
    _weekEnd.layer.borderColor = UIColorFromRGB(0X008000).CGColor;
    _weekDay.layer.borderColor = UIColorFromRGB(0Xffffff).CGColor;
    _everyDa.layer.borderColor = UIColorFromRGB(0Xffffff).CGColor;
    weekDay = 3;
}

@end
