//
//  IServeWebConstant.m
//  iServePro
//
//  Created by Rahul Sharma on 25/02/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//


//Booking Type
NSString *constkNotificationTypeBookingType  =@"1";
NSString *constkNotificationTypeBookingAccept  =@"2";
NSString *constkNotificationTypeBookingReject  =@"3";
NSString *constkNotificationTypeBookingOnTheWay =@"5";
NSString *constkNotificationTypeBookingArrived  =@"6";
NSString *constkNotificationTypeBookingDropped  =@"7";

#define Process @"process.php/"
//Methods
NSString *const MethodGetCarType                      = Process @"getTypes";
NSString *const MethodPatientSignUp                   =Process  @"masterSignup1";
NSString *const MethodPatientLogin                    =Process @"masterLogin";
NSString *const MethodDoctorUploadImage               =Process @"uploadImage";
NSString *const MethodAppointments                    =@"getMasterAppointments";
NSString *const MethodGetPendingAppointments          =@"getPendingAppointments";
NSString *const MethodAppointmentsHistoryWithPatients =@"getHistoryWith";
NSString *const MethodRespondToAppointMent            =@"respondToAppointment";
NSString *const MethodGetMySlotes                     =Process @"getMySlots";
NSString *const MethodupdateApptStatus                =@"updateApptStatus";
NSString *const MethodGetMasterProfile                =Process @"getMasterProfile";
NSString *const MethodResetPassword                   =Process @"resetPassword";
NSString *const MethodLogout                          =Process @"logout";
NSString *const MethodAppointmentDetail               =Process @"getAppointmentDetails";
NSString *const MethodUploadImage                     =@"uploadImage";
NSString *const MethodUpdateMasterStaus               = @"updateMasterStatus";
NSString *const MethodGetAppointmentDetails            = @"getApptDetails";
NSString *const MethodabortAppointment               = @"abortAppointment";



// SMP-Service Method Parameter

//SignUp

NSString *kSMPSignUpFirstName                  = @"ent_first_name";
NSString *kSMPSignUpLastName                   = @"ent_last_name";
NSString *kSMPSignUpMobile                     = @"ent_mobile";
NSString *kSMPSignUpEmail                      = @"ent_email";
NSString *kSMPSignUpPassword                   = @"ent_password";
NSString *kSMPSignUpAddLine1                   = @"ent_address_line1";
NSString *kSMPSignUpAddLine2                   = @"ent_address_line2";
NSString *kSMPSignUpAccessToken                = @"ent_token";
NSString *kSMPSignUpDateTime                   = @"ent_date_time";
NSString *kSMPSignUpCountry                    = @"ent_country";
NSString *kSMPSignUpCity                       = @"ent_city";
NSString *kSMPSignUpDeviceType                 = @"ent_device_type";
NSString *kSMPSignUpDeviceId                   = @"ent_dev_id";
NSString *kSMPSignUpPushToken                  = @"ent_push_token";
NSString *kSMPSignUpZipCode                    = @"ent_zipcode";
NSString *kSMPSignUpCreditCardNo               = @"ent_cc_num";
NSString *kSMPSignUpCreditCardCVV              = @"ent_cc_cvv";
NSString *kSMPSignUpCreditCardExpiry           = @"ent_cc_exp";
NSString *kSMPSignUpTandC                      = @"ent_terms_cond";
NSString *kSMPSignUpPricing                    = @"ent_pricing_cond";
NSString *kSMPSignUpLattitude                  = @"ent_latitude";
NSString *kSMPSignUpLongitude                  = @"ent_longitude";
NSString *kSMPSignUpSeatCapcity                = @"ent_seat_cap";
NSString *kSMPSignUpRegistartionNumber         = @"ent_reg_num";
NSString *kSMPSignUpLicenceNumber              = @"ent_dd_num";
//NSString *kSMPSignUpCarType                    = @"ent_service_type";
NSString *kSMPSignupCompneyId                  = @"ent_comp_id";
NSString *kSMPSignupTaxNumber                  = @"ent_tax_num";
NSString *kSMPSignupCompneyname                = @"ent_comp_name";
NSString *kSMPSignupDoctorType                = @"ent_service_type";


//Request Params For Login

NSString *kSMPLoginEmail                       = @"ent_email";
NSString *kSMPLoginPassword                    = @"ent_password";
NSString *kSMPLoginDeviceType                  = @"ent_device_type";
NSString *kSMPLoginDevideId                    = @"ent_dev_id";
NSString *kSMPLoginPushToken                   = @"ent_push_token";
NSString *kSMPLoginCarId                       = @"ent_car_id";


//Request for Upload Image
NSString *kSMPUploadDeviceId                     = @"ent_dev_id";
NSString *kSMPUploadSessionToken                 = @"ent_sess_token";
NSString *kSMPUploadImageName                    = @"ent_snap_name";
NSString *kSMPUploadImageChunck                  = @"ent_snap_chunk";
NSString *kSMPUploadfrom                         = @"ent_upld_from";
NSString *kSMPUploadtype                         = @"ent_snap_type";
NSString *kSMPUploadDateTime                     = @"ent_date_time";
NSString *kSMPUploadOffset                       = @"ent_offset";

//params for Firstname
NSString *kSMPFirstName                          = @"ent_first_name";
NSString *kSMPLastName                           = @"ent_last_name";
NSString *kSMPEmail                              = @"ent_email";
NSString *kSMPPhoneNo                            = @"ent_mobile";
NSString *kSMPPassword                           = @"ent_password";


// Common params

NSString *kSMPCommonDeviceType                 =@"ent_device_type";
NSString *kSMPCommonDevideId                   =@"ent_dev_id";
NSString *kSMPCommonPushToken                  =@"ent_push_token";
NSString *kSMPCommonUpDateTime                  =@"ent_date_time";

//Parsms for checking user loged out or not

NSString *kSMPcheckUserId                        = @"user_id";
NSString *kSMPcheckUserSessionToken              = @"ent_sess_token";
NSString *kSMPgetPushToken                       = @"ent_push_token";


//Requset for GetAppointment
NSString *kSMPAppointmentDate                  =@"ent_appnt_dt";

// Logout the user

NSString *kSMPLogoutSessionToken                = @"user_session_token";
NSString *kSMPLogoutUserId                      = @"logout_user_id";

//Requset for AcceptRejectBooking
NSString *kSMPRespondPassengerEmail                   =@"ent_pat_email";
NSString *kSMPRespondBookingDateTime                  =@"ent_appnt_dt";
NSString *kSMPRespondResponse                         =@"ent_response";
NSString *kSMPRespondBookingType                      =@"ent_book_type";
NSString *kSMPRespondDocNotes                         =@"ent_doc_remarks";

//request For Passenger Deatil
NSString *kSMPPassengerUserType                         =@"ent_user_type";

//Request Update Doctor Status
NSString *KSMPMasterStatus                             = @"ent_status";
NSString *KSMPPatientEmail                             =@"ent_pat_email";

